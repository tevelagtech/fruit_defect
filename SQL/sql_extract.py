import sqlite3

# db_file = sqlite3.connect('C:/Users/Nadav/Desktop/fruits_simulation_image_1603365543.8.db')

def create_connection(db_file):
    """ create a database connection to the SQLite database
        specified by the db_file
    :param db_file: database file
    :return: Connection object or None
    """
    conn = None
    try:
        conn = sqlite3.connect(db_file)
    except Error as e:
        print(e)

    return conn

def select_task(conn, ):

    cur = conn.cursor()
    cur.execute("SELECT picked, cluster_id, momentum_object_mass, z_end, radius from pick_missions where picked == 1;")
    rows = cur.fetchall()
    with open('C:/Users/Nadav/Desktop/selected.sql', 'w') as file:
        file.write(rows)
    file.close()

    for row in rows:
        print(row)
def main():
    database = r"C:/Users/Nadav/Desktop/fruits_simulation_image_1603365543.8.db"

    # create a database connection
    conn = create_connection(database)
    with conn:
        select_task(conn)

if __name__ == '__main__':
    main()
    CREATE
    TABLE
    picked
    AS
    SELECT * FROM
    pick_missions
    WHERE
    picked == 1;