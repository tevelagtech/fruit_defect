import cv2
import matplotlib.pyplot as plt
import numpy as np

def save_frame(frame, path):

    cv2.imwrite(path, frame)

# def select_fruit_pixels_by_mask(bgr_path, mask_path):
#     bgr = cv2.imread(bgr_path)
#     mask = cv2.imread(mask_path)
#
#     mask[mask > 0] = 1
#     binary_mask = np.sum(mask, axis=-1)
#     binary_mask[binary_mask > 0] = 1
#     binary_mask = np.expand_dims(binary_mask, 2)
#     binary_mask = np.concatenate((binary_mask, binary_mask, binary_mask), axis=-1)
#     mask_mult_bgr = (bgr * binary_mask).astype('uint8')

    # cv2.imshow('', mask_mult_bgr)
    # cv2.waitKey(0)
    #
    # b, g, r = mask_mult_bgr[..., 0], mask_mult_bgr[..., 1], mask_mult_bgr[..., 2]
    #
    # b_valid = b[b!=0]
    # g_valid = g[b!=0]
    # r_valid = r[b!=0]
    #
    # plt.hist(g_valid, bins=255)
    # plt.show()


if __name__ == '__main__':

    # select_fruit_pixels_by_mask(bgr_path='Desktop/1.png',
    #                             mask_path='/home/yotam/stark_bags/task_nadav manual segmentation-2020_09_13_12_20_39-segmentation mask 1.1/SegmentationObject/nadav_example_1.png')

    vid_path = 'C:/Users/Nadav/Desktop/25/vid_200909_102257_C.avi'
    frame = [17,25,36,38,75,77,121,125,157,160,173,174,182,185,216,217,240,243]
    start_frame = [i * 30 for i in frame]
    count = 1
    for frame in start_frame:
        cap = cv2.VideoCapture(vid_path)
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame)

        ret, frame = cap.read()
        if ret:

            save_frame(frame, 'C:/Users/Nadav/Desktop/25/nadav_example_%d.png' % count)

        count += 1
        # bbox = cv2.selectROI('', frame, False, False)
        # y, x, h, w = bbox[1], bbox[0], bbox[3], bbox[2]
        # # frame_w_bbox = cv2.rectangle(frame, (x, y), (x+w, y+h), (255, 0, 0), 3)
        # crop = frame[y:y+h, x:x+w]
        # b, g, r = frame[..., 0], frame[..., 1], frame[..., 2]
        #
        # cv2.imshow('', crop)
        # cv2.waitKey(0)

    # b_valid = b[b!=0]
    # g_valid = g[r!=0]
    # r_valid = r[b!=0]
    #
    # plt.hist(r_valid, bins=255)
    # plt.show()
