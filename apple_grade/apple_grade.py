import numpy as np
import matplotlib.pyplot as plt
import cv2
import csv

if __name__ == '__main__':

    file = open('C:/Users/Nadav/Desktop/list.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(["SN", "EXR", "Rn", "meanL(HSL)", "SD_Cb(YCbCr)", "CIVE", "RBI", "grade"])


    def parameters(img, name, key):
        b, g, r = cv2.split(img)
        Rn = np.mean(r) / np.mean(r + g + b)  # Normalized red of RGB (Rn)
        Gn = np.mean(g) / np.mean(r + g + b)  # Normalized green of RGB(Gn)
        Bn = np.mean(b) / np.mean(r + g + b)  # Normalized blue of RGB(Bn)
        hsl = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)  # HSL color module
        meanL = np.mean(hsl[:, :, 1])  # mean of L in HSL
        ycbcr = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)  # YCbCr color module
        sdCb = np.std(ycbcr[:, :, 2])  # SD of Cb in YCbCr
        EXR = 1.4 * Rn - Gn  # excess red
        cive = 0.441 * Rn - 0.811 * Gn + 0.385 * Bn + 18.78  # Color index for extracted vegetation cover(CIVE)
        rbi = (Rn - Bn) / (Rn + Bn)  # Red-blue contrast (RBI)
        writer.writerow([name, EXR, Rn, meanL, sdCb, cive, rbi, key - 48])


    count = 3
    while count > 0:
        pic = cv2.imread("C:/Users/Nadav/Desktop/New folder/%d.png" % count)
        cv2.imshow('%d' % count, pic)
        key = cv2.waitKey(0)
        parameters(pic, '%d' % count, key)
        cv2.destroyAllWindows()
        count -= 1
