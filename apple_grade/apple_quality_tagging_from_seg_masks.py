import os
import cv2
import numpy as np
import csv


def parameters(img, name, key):
    b, g, r = cv2.split(img)
    b1 = b[(b != 0) & (g != 0) & (r != 0)]
    g1 = g[(b != 0) & (g != 0) & (r != 0)]
    r1 = r[(b != 0) & (g != 0) & (r != 0)]
    Rn = np.mean(r1) / np.mean(r1 + g1 + b1)  # Normalized red of RGB (Rn)
    Gn = np.mean(g1) / np.mean(r1 + g1 + b1)  # Normalized green of RGB(Gn)
    Bn = np.mean(b1) / np.mean(r1 + g1 + b1)  # Normalized blue of RGB(Bn)

    hsl = cv2.cvtColor(img, cv2.COLOR_BGR2HLS)  # HSL color module
    h, l, s = cv2.split(hsl)
    h1 = h[(h != 0) & (l != 0) & (s != 0)]
    l1 = l[(h != 0) & (l != 0) & (s != 0)]
    s1 = s[(h != 0) & (l != 0) & (s != 0)]
    meanL = np.mean(l1)  # mean of L in HSL

    ycbcr = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)  # YCbCr color module
    y, cr, cb = cv2.split(ycbcr)
    y1 = y[(y != 0) & (cb != 0) & (cr != 0)]
    cb1 = cb[(y != 0) & (cb != 0) & (cr != 0)]
    cr1 = s[(y != 0) & (cb != 0) & (cr != 0)]
    sdCb = np.std(cb1)  # SD of Cb in YCbCr

    lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)  # Lab color module
    L, a, bu = cv2.split(lab)
    L1 = L[(L != 0) & (a != 0) & (bu != 0)]
    a1 = a[(L != 0) & (a != 0) & (bu != 0)]
    bu1 = b[(L != 0) & (a != 0) & (bu != 0)]
    meanA = np.mean(a1)  # mean of a in Lab
    meanB = np.mean(bu1)  # mean of b in Lab

    EXR = 1.4 * Rn - Gn  # excess red
    cive = 0.441 * Rn - 0.811 * Gn + 0.385 * Bn + 18.78  # Color index for extracted vegetation cover(CIVE)
    rbi = (Rn - Bn) / (Rn + Bn)  # Red-blue contrast (RBI)
    writer.writerow([name, EXR, Rn, meanL, meanA, meanB, sdCb, cive, rbi, key - 48])
if __name__ == '__main__':
    imgs_path = '/home/nadav/Desktop/try/rgb/'  # TODO
    masks_path = '/home/nadav/Desktop/try/semantic_masks/'  # TODO

    images_suffix = '.png'
    masks_suffix = '.png'

    imgs_fnames = [x[:x.index(images_suffix)] for x in sorted(os.listdir(imgs_path))]
    masks_fnames = [x[:x.index(masks_suffix)] for x in sorted(os.listdir(masks_path))]
    fnames = list(set(imgs_fnames) & set(masks_fnames))

    file = open('/home/nadav/Desktop/try/list.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(["SN", "EXR", "Rn", "meanL(HSL)", "meanA(Lab)", "meanB(Lab)", "SD_Cb(YCbCr)", "CIVE", "RBI", "grade"])



    for fname in fnames:
        print (fname)
        img_fname = fname + images_suffix
        mask_fname = fname + masks_suffix

        bgr = cv2.imread(os.path.join(imgs_path, img_fname))
        mask = cv2.imread(os.path.join(masks_path, mask_fname))

        unique_mask_bgr_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)
        count = 1
        for unq_bgr_val in unique_mask_bgr_vals:
            if np.all(unq_bgr_val == 0):
                continue

            single_fruit_mask = np.all(mask == unq_bgr_val, axis=-1).astype(np.uint8) * 255
            contours = cv2.findContours(single_fruit_mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0][0]
            x, y, w, h = cv2.boundingRect(contours)  # x_left, y_up, width, height

            if w < 10 or h < 10:  # TODO change to fit your needs
                continue

            cv2.rectangle(bgr, (x - 6, y - 6), (x + w + 6, y + h + 6), (0, 255, 0), 2)
            cv2.imshow('', bgr)
            key = cv2.waitKey(0)
            cv2.destroyAllWindows()
            bgr = cv2.imread(os.path.join(imgs_path, img_fname))
            finame = fname + '_' + str(count) + '_Grade - ' + str(key-48)
            cv2.imshow('', bgr)
            # TODO this is your crops, you can enlarge them, save them, annotate them, etc
            bgr_crop = bgr[y+3:y + (h-3), x+3:x + (w-3), :]

            single_fruit_mask = np.all(mask == unq_bgr_val, axis=-1).astype(np.uint8)
            index_too_keep = (single_fruit_mask == 1)
            single_fruit_mask = (single_fruit_mask).astype(np.uint8)
            rgb_masked = bgr[index_too_keep, :]
            cv2.imwrite('/home/nadav/Desktop/try/', rgb_masked)


            mask_crop = single_fruit_mask[y:y + h, x:x+w]
            cv2.imwrite('/home/nadav/Desktop/try/gala/%s.png' % finame, single_fruit_mask)
            count += 1

            # single_fruit_mask = (single_fruit_mask / 255).astype(np.uint8)
            # bgr_where_mask = bgr[y:y+h, x:x+w] *\
            #                  np.concatenate((np.expand_dims(single_fruit_mask[y:y+h, x:x+w], 2),
            #                                  np.expand_dims(single_fruit_mask[y:y+h, x:x+w], 2),
            #                                  np.expand_dims(single_fruit_mask[y:y+h, x:x+w], 2)),
            #                                 axis=-1)
            # cv2.imwrite('C:/Users/Nadav/Desktop/1.png', bgr_where_mask)
            if key != 113:
                parameters(rgb_masked, img_fname, key)
            # cv2.imshow('', bgr_where_mask)
            # cv2.waitKey(0)

        # cv2.imshow('', bgr)
        # cv2.waitKey(0)
