import os
import cv2
import numpy as np
import csv


def parameters(bgr_vector, name, key):
    # b, g, r = cv2.split(img)
    b, g, r = bgr_vector[:, 0], bgr_vector[:, 1], bgr_vector[:, 2]
    # b1 = b[(b != 0) & (g != 0) & (r != 0)]
    # g1 = g[(b != 0) & (g != 0) & (r != 0)]
    # r1 = r[(b != 0) & (g != 0) & (r != 0)]
    Rn = np.mean(r) / np.mean(r + g + b)  # Normalized red of RGB (Rn)
    Gn = np.mean(g) / np.mean(r + g + b)  # Normalized green of RGB(Gn)
    Bn = np.mean(b) / np.mean(r + g + b)  # Normalized blue of RGB(Bn)

    # hsl = cv2.cvtColor(bgr_vector, cv2.COLOR_BGR2HLS)  # HSL color module
    # h, l, s = cv2.split(hsl)
    hls = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2HLS).reshape((-1, 3))
    h, l, s = hls[:, 0], hls[:, 1], hls[:, 2]
    # h1 = h[(h != 0) & (l != 0) & (s != 0)]
    # l1 = l[(h != 0) & (l != 0) & (s != 0)]
    # s1 = s[(h != 0) & (l != 0) & (s != 0)]
    meanL = np.mean(l)  # mean of L in HSL
    meanH = np.mean(h)
    meanS = np.mean(s)

    # ycbcr = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)  # YCbCr color module
    # y, cr, cb = cv2.split(ycbcr)
    ycbcr = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2YCrCb).reshape((-1, 3))
    y, cr, cb = ycbcr[:, 0], ycbcr[:, 1], ycbcr[:, 2]
    # y1 = y[(y != 0) & (cb != 0) & (cr != 0)]
    # cb1 = cb[(y != 0) & (cb != 0) & (cr != 0)]
    # cr1 = s[(y != 0) & (cb != 0) & (cr != 0)]
    sdCb = np.std(cb)  # SD of Cb in YCbCr
    meanY = np.mean(y)  # mean of L in HSL
    meanCb = np.mean(cb)
    meanCr = np.mean(cr)

    # lab = cv2.cvtColor(img, cv2.COLOR_BGR2LAB)  # Lab color module
    # L, a, bu = cv2.split(lab)
    lab = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2LAB).reshape((-1, 3))
    L, a, bu = lab[:, 0], lab[:, 1], lab[:, 2]
    # L1 = L[(L != 0) & (a != 0) & (bu != 0)]
    # a1 = a[(L != 0) & (a != 0) & (bu != 0)]
    # bu1 = b[(L != 0) & (a != 0) & (bu != 0)]
    meanA = np.mean(a)  # mean of a in Lab
    meanB = np.mean(bu)  # mean of b in Lab
    meanLa = np.mean(L)
    EXR = 1.4 * Rn - Gn  # excess red
    cive = 0.441 * Rn - 0.811 * Gn + 0.385 * Bn + 18.78  # Color index for extracted vegetation cover(CIVE)
    rbi = (Rn - Bn) / (Rn + Bn)  # Red-blue contrast (RBI)
    writer.writerow([name, EXR, Rn, Gn, Bn, meanL,meanH,meanS,meanLa,meanA, meanB,
                     meanCb,meanCr,meanY, sdCb, cive, rbi, key - 48])
if __name__ == '__main__':
    imgs_path = '/home/nadav/Desktop/try/rgb/'  # TODO
    masks_path = '/home/nadav/Desktop/try/semantic_masks/'  # TODO

    images_suffix = '.png'
    masks_suffix = '.png'

    imgs_fnames = [x[:x.index(images_suffix)] for x in sorted(os.listdir(imgs_path))]
    masks_fnames = [x[:x.index(masks_suffix)] for x in sorted(os.listdir(masks_path))]
    fnames = list(set(imgs_fnames) & set(masks_fnames))

    file = open('/home/nadav/Desktop/try/list.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(["SN",'EXR','Rn', 'Gn','Bn', 'meanL(hsl)','meanH(hsl)','meanS(hsl)','meanLa(lab)', 'meanA(lab)', 'meanB(lab)',
                     'meanCb','meanCr','meanY', 'sdCb', 'cive', 'rbi', "grade"])



    for fname in fnames:
        print (fname)
        img_fname = fname + images_suffix
        mask_fname = fname + masks_suffix

        bgr = cv2.imread(os.path.join(imgs_path, img_fname))
        mask = cv2.imread(os.path.join(masks_path, mask_fname))

        unique_mask_bgr_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)
        count = 1
        for unq_bgr_val in unique_mask_bgr_vals:
            if np.all(unq_bgr_val == 0):
                continue


            single_fruit_mask = np.all(mask == unq_bgr_val, axis=-1).astype(np.uint8) * 255
            ys, xs = np.where(single_fruit_mask != 0)
            x, x2 = np.min(xs), np.max(xs)
            y, y2 = np.min(ys), np.max(ys)
            w = x2 - x
            h = y2 - y

            #
            if w < 15 or h < 15:  # TODO change to fit your needs
                continue

            cv2.rectangle(bgr, (x - 6, y - 6), (x + w + 6, y + h + 6), (0, 255, 0), 2)
            cv2.imshow('', bgr)
            key = cv2.waitKey(0)
            # cv2.destroyAllWindows()
            bgr = cv2.imread(os.path.join(imgs_path, img_fname))
            finame = fname + '_' + str(count) + '_Grade - ' + str(key-48)
            cv2.imshow('', bgr)


            single_fruit_mask = np.all(mask == unq_bgr_val, axis=-1).astype(np.uint8)
            index_too_keep = (single_fruit_mask == 1)
            bgr_masked = np.copy(bgr)
            bgr_masked[np.invert(index_too_keep)] = 0
            single_fruit_mask = (single_fruit_mask).astype(np.uint8)
            bgr_masked_vector = bgr[index_too_keep, :]
            cv2.imwrite('/home/nadav/Desktop/try/1.png', bgr_masked)

            cv2.imwrite('/home/nadav/Desktop/try/gala/%s.png' % finame, single_fruit_mask)
            count += 1




            if key != ord('q'):
                parameters(bgr_masked_vector, img_fname, key)

            else:
                quit(0)
