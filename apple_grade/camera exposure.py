import cv2
import numpy as np
import pyrealsense2 as rs
import random
import colour
import skimage




from sklearn.cross_decomposition import PLSRegression

no_white =[[181, 183, 70], [182, 49, 40], [49, 149, 69], [3, 80, 139], [37, 162, 199], [201, 158, 65], [191, 145, 181], [66, 77, 123]]
mask= cv2.imread('/home/nadav/Desktop/maskexpo.png')
unique_mask_rgb_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)
unique_mask_rgb_vals = unique_mask_rgb_vals[1:]
def unique_count_app(a):
    r, g, b = a[:, 0], a[:, 1], a[:, 2]
    # pixels2delete = (r > 235) | (g > 235) | (b > 235)
    pixels2delete = (r > 235) & (g > 235) & (b > 235)
    pixels2save = np.bitwise_not(pixels2delete)
    a = a[pixels2save]
    common = np.mean(a.reshape((-1, 3)), axis=0).astype(np.int)
    # common = np.median(a.reshape((-1, 3)), axis=0).astype(np.int)
    # colors, count = np.unique(a.reshape(-1,a.shape[-1]), axis=0, return_counts=True)
    # return list(colors[count.argmax()])
    return list(common)
# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)

# Start streaming
pipeline.start(config)
try:
    count = 0
    # Get the sensor once at the beginning. (Sensor index: 1)
    sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
    sensor.set_option(rs.option.exposure, 3)
    exposure = sensor.get_option(rs.option.exposure)

    while True:
        count = count + 1
        colors = []
        # r = random.randint(10, 1000)
        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames(100000000)

        color_frame = frames.get_color_frame()
        bgr = np.asanyarray(color_frame.get_data())
        color_frame = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
        for unq_rgb_val in unique_mask_rgb_vals:
            #     if np.all(unq_rgb_val == 0):
            #         continue

            # print ('after np all',time.time() - start)
            single_fruit_mask = np.all(mask == unq_rgb_val, axis=-1).astype(np.uint8)
            index_too_keep = (single_fruit_mask == 1)
            single_fruit_mask = (single_fruit_mask).astype(np.uint8)
            rgb_masked = color_frame[index_too_keep, :]
            colors.append(unique_count_app(rgb_masked))
        # print(colors)
        # start = time.time()
        pls = PLSRegression(n_components=3)
        colors4regression = []
        test1_colors4regression = []

        colors_discarded = []
        for index, (rgb_triplet, test1_triplet) in enumerate(zip(colors, no_white)):
            if np.all(np.array(rgb_triplet) > 0):
                colors4regression.append(rgb_triplet)
                test1_colors4regression.append(test1_triplet)

        colors4regression = np.array(colors4regression)
        test1_colors4regression = np.array(test1_colors4regression)
        if len(colors4regression) < 3:
            #continue
            score = 0.95
            sensor.set_option(rs.option.enable_auto_exposure,100)
            # if exposure>1:
            #     exposure += 10
            #     sensor.set_option(rs.option.exposure, exposure)
            print('n_colors: {}'.format(len(colors4regression)))
        else:
            pls.fit(colors4regression, test1_colors4regression)
            score = pls.score(colors4regression, test1_colors4regression)
        # print(score)
        # # Get the sensor once at the beginning. (Sensor index: 1)
        # sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
        # # Set the exposure anytime during the operation
        if count % 3 == 0:
            # too bright
            if score < 0.90 :  # or score >0.99:
                if exposure>1:
                    exposure += 1
                    sensor.set_option(rs.option.exposure, exposure)
            # TODO: too dark
            elif score > 0.9999:
                if exposure>1:
                    exposure += 50
                    sensor.set_option(rs.option.exposure, exposure)
            # good exposure
            else:
                pass
                # exposure = 200
                # sensor.set_option(rs.option.exposure, exposure)
            # else:
            #     sensor.set_option(rs.option.enable_auto_exposure, 100)
        exp = sensor.get_option(rs.option.exposure)
        print("exposure = %d" % exp)
        cv2.putText(color_frame, 'score: {}'.format(score), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
        cv2.putText(color_frame, 'exposure: {}'.format(exposure), (30, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
        cv2.putText(color_frame, 'n_colors: {}'.format(len(colors4regression)), (30, 90), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2)
        # Show images
        cv2.imshow('RealSense', cv2.cvtColor(color_frame,cv2.COLOR_BGR2RGB))
        # cv2.waitKey(1)
    #Convert image from int to float
        Float_image=skimage.img_as_float(color_frame)

        #Normalise image to have pixel values from 0 to 1
        Normalised_image = (Float_image - np.min(Float_image))/np.ptp(Float_image)

        #Decoded the image with sRGB EOTF
        Decoded_img = colour.models.eotf_sRGB(Normalised_image)

        #Performed Finlayson 2015 color correction to linear data:
        for im in Decoded_img:
            im[:] = colour.colour_correction(im[:],colors4regression,test1_colors4regression, "Finlayson 2015")
        # print('after correction', time.time() - start)
            
        #Encoded image back to sRGB
        Encoded_img=colour.models.eotf_inverse_sRGB(Decoded_img)

        #Denormalized image to fit 255 pixel values
        Denormalized_image=Encoded_img*255

        #Converted floats back to integers
        Integer_image= Denormalized_image.astype(int)
        # Integer_image = cv2.cvtColor(Integer_image,cv2.COLOR_BGR2RGB)
        # cv2.imwrite('/home/nadav/Desktop/calsun_video.png', Integer_image)

        display_image = cv2.cvtColor(np.clip(Integer_image, 0, 255).astype(np.uint8), cv2.COLOR_RGB2BGR)
        # display_image = cv2.putText(display_image, 'colors used: {}'.format(len(colors4regression)), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)

        start_line = 100
        for triplet_found, triplet_real in zip(colors4regression, test1_colors4regression):
            display_image = cv2.circle(display_image, (550, start_line), 1, triplet_found[::-1].astype('uint8').tolist(), 30)
            display_image = cv2.circle(display_image, (600, start_line), 1, triplet_real[::-1].astype('uint8').tolist(), 30)
            start_line += 50
        # out.write(display_image)
        cv2.imshow('frame', display_image)
        cv2.waitKey(1)
finally:

    # Stop streaming
    pipeline.stop()