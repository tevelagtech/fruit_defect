import os
import cv2
import numpy as np
import pandas as pd
import csv
import  matplotlib
import sys
import time
matplotlib.use('TKAgg')
import shutil

def parameters(bgr_vector, name, key):
    b, g, r = bgr_vector[:, 0], bgr_vector[:, 1], bgr_vector[:, 2]
    b = b.astype(np.float)
    g = g.astype(np.float)
    r = r.astype(np.float)
    Rn = np.mean(r) / np.mean(r + g + b)  # Normalized red of RGB (Rn)
    Gn = np.mean(g) / np.mean(r + g + b)  # Normalized green of RGB(Gn)
    Bn = np.mean(b) / np.mean(r + g + b)  # Normalized blue of RGB(Bn)

    hls = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2HLS).reshape((-1, 3))
    h, l, s = hls[:, 0].astype(np.float), hls[:, 1].astype(np.float), hls[:, 2].astype(np.float)
    meanL = np.mean(l)  # mean of L in HSL
    meanH = np.mean(h)
    meanS = np.mean(s)

    ycbcr = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2YCrCb).reshape((-1, 3))
    y, cr, cb = ycbcr[:, 0].astype(np.float), ycbcr[:, 1].astype(np.float), ycbcr[:, 2].astype(np.float)
    sdCb = np.std(cb)  # SD of Cb in YCbCr
    meanY = np.mean(y)  # mean of L in HSL
    meanCb = np.mean(cb)
    meanCr = np.mean(cr)

    lab = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2LAB).reshape((-1, 3))
    L, a, bu = lab[:, 0].astype(np.float), lab[:, 1].astype(np.float), lab[:, 2].astype(np.float)
    meanA = np.mean(a)  # mean of a in Lab
    meanB = np.mean(bu)  # mean of b in Lab
    meanLa = np.mean(L)
    hue_angle = np.mean(np.arctan(bu/a))


    EXR = 1.4 * Rn - Gn  # excess red
    cive = 0.441 * Rn - 0.811 * Gn + 0.385 * Bn + 18.78  # Color index for extracted vegetation cover(CIVE)
    rbi = (Rn - Bn) / (Rn + Bn)  # Red-blue contrast (RBI)

    writer.writerow([name, EXR, Rn, Gn, Bn, meanL,meanH,meanS,meanLa,meanA, meanB,
                     meanCb,meanCr,meanY, sdCb, cive, rbi, hue_angle, key - 48])
def parse2full_paths(rgb_path, mask_path, ann_path):
    rgb_name_suffix = np.array([os.path.splitext(x) for x in os.listdir(rgb_path)])
    rgb_fname = rgb_name_suffix[:, 0]
    rgb_suffix = rgb_name_suffix[:, 1]

    mask_name_suffix = np.array([os.path.splitext(x) for x in os.listdir(mask_path)])
    mask_fname = mask_name_suffix[:, 0]
    mask_suffix = mask_name_suffix[:, 1]

    ann_name_suffix = np.array([os.path.splitext(x) for x in os.listdir(ann_path)])
    ann_fname = ann_name_suffix[:, 0]
    ann_suffix = ann_name_suffix[:, 1]

    rgb_indices2keep = [i for i, x in enumerate(rgb_fname) if x in mask_fname and x in ann_fname]
    mask_indices2keep = [i for i, x in enumerate(mask_fname) if x in rgb_fname and x in ann_fname]
    ann_indices2keep = [i for i, x in enumerate(ann_fname) if x in rgb_fname and x in mask_fname]

    rgb_fname = rgb_fname[rgb_indices2keep].tolist()
    rgb_suffix = rgb_suffix[rgb_indices2keep].tolist()
    mask_fname = mask_fname[mask_indices2keep].tolist()
    mask_suffix = mask_suffix[rgb_indices2keep].tolist()
    ann_fname = ann_fname[ann_indices2keep].tolist()
    ann_suffix = ann_suffix[rgb_indices2keep].tolist()

    rgb_full_paths = [os.path.join(rgb_path, x + y) for x, y in zip(rgb_fname, rgb_suffix)]
    mask_full_paths = [os.path.join(mask_path, x + y) for x, y in zip(mask_fname, mask_suffix)]
    ann_full_paths = [os.path.join(ann_path, x + y) for x, y in zip(ann_fname, ann_suffix)]

    rgb_full_paths = sorted(rgb_full_paths)
    mask_full_paths = sorted(mask_full_paths)
    ann_full_paths = sorted(ann_full_paths)

    return rgb_full_paths, mask_full_paths, ann_full_paths


if __name__ == '__main__':
    rgb_path = 'C:/Users/Nadav/Desktop/10.12.18_ramat3_C/rgb'
    mask_path = 'C:/Users/Nadav/Desktop/10.12.18_ramat3_C/mask'
    ann_path = 'C:/Users/Nadav/Desktop/10.12.18_ramat3_C/ann'

    # ---------------------
    # start
    # ---------------------
    file = open('C:/Users/Nadav/Desktop/10.12.18_ramat3_C/10.12.18_ramat3_C.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow(
        ["SN", 'EXR', 'Rn', 'Gn', 'Bn', 'meanL(hsl)', 'meanH(hsl)', 'meanS(hsl)', 'meanLa(lab)', 'meanA(lab)',
         'meanB(lab)', 'meanCb', 'meanCr', 'meanY', 'sdCb', 'cive', 'rbi', 'hue angle', 'grade'])
    rgb_full_paths, mask_full_paths, ann_full_paths = parse2full_paths(rgb_path, mask_path, ann_path)
    for rgb_file_path, mask_file_path, ann_file_path in zip(rgb_full_paths, mask_full_paths, ann_full_paths):

        bgr = cv2.imread(rgb_file_path)
        # hsv = cv2.cvtColor(bgr,cv2.RGB)
        mask = cv2.imread(mask_file_path)
        ann_file = pd.read_csv(ann_file_path)
        count = 1
        for x1x2y1y2 in ann_file.values:
            bgr = cv2.imread(rgb_file_path)
            x1, x2, y1, y2 = x1x2y1y2
            x, y = x1, y1
            w, h = x2 - x1, y2 - y1
            if w < 30 or h < 30:
                continue

            margin = 0
            bgr_crop = bgr[y1+margin:y2-margin, x1+margin:x2-margin, :]
            mask_crop = mask[y1+margin:y2-margin, x1+margin:x2-margin]

            bgr_3d_vector = bgr_crop[mask_crop[..., 0] != 0]   # [[b, g, r] * n]
            b, g, r = bgr_3d_vector[:, 0], bgr_3d_vector[:, 1], bgr_3d_vector[:, 2]
            pixels_too_bright = (r > 170) & (g > 170) & (b > 170)
            pixels_too_dark = (r < 80) & (g < 80) & (b < 80)
            a = bgr_3d_vector[pixels_too_bright]
            b = bgr_3d_vector[pixels_too_dark]
            if len(bgr_3d_vector) == 0 or float(len(a))/len(bgr_3d_vector)>0.1 or float(len(b))/len(bgr_3d_vector)>0.1:
                continue
            bgr_cpy = np.copy(bgr)
            cv2.rectangle(bgr_cpy, (x1, y1), (x2, y2), (0, 255, 0), 1)
            cv2.imshow('', bgr_cpy)
            key = cv2.waitKey(0)
            if key == ord('b'):
                file.close()
                time.sleep(1)
                sys.exit(0)
            if key != 113 and key !=-1 :
                head, tail = os.path.split(rgb_file_path)

                parameters(bgr_3d_vector, tail, key)
                cv2.imwrite('C:/Users/Nadav/Desktop/10.12.18_ramat3_C/done/crop/rgb/'+'Grade_'+str(key-48)+tail+str(count)+'.png', bgr_crop)
                cv2.imwrite('C:/Users/Nadav/Desktop/10.12.18_ramat3_C/done/crop/mask/'+'Grade_'+str(key-48)+tail+str(count)+'.png', mask_crop)
            count += 1
        head, tail = os.path.split(rgb_file_path)
        shutil.move(rgb_file_path, "C:/Users/Nadav/Desktop/10.12.18_ramat3_C/done/rgb/"+tail)
        head, tail = os.path.split(mask_file_path)
        shutil.move(mask_file_path, "C:/Users/Nadav/Desktop/10.12.18_ramat3_C/done/mask/"+tail)
        head, tail = os.path.split(ann_file_path)
        shutil.move(ann_file_path, "C:/Users/Nadav/Desktop/10.12.18_ramat3_C/done/ann/"+tail)



