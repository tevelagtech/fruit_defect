import os
import cv2
import numpy as np
import pandas as pd
import csv
import  matplotlib
import sys
import time
matplotlib.use('TKAgg')
import shutil
def parameters(bgr_vector, name, key):
    b, g, r = bgr_vector[:, 0], bgr_vector[:, 1], bgr_vector[:, 2]
    b = b.astype(np.float)
    g = g.astype(np.float)
    r = r.astype(np.float)
    Rn = np.mean(r) / np.mean(r + g + b)  # Normalized red of RGB (Rn)
    Gn = np.mean(g) / np.mean(r + g + b)  # Normalized green of RGB(Gn)
    Bn = np.mean(b) / np.mean(r + g + b)  # Normalized blue of RGB(Bn)

    hls = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2HLS).reshape((-1, 3))
    h, l, s = hls[:, 0].astype(np.float), hls[:, 1].astype(np.float), hls[:, 2].astype(np.float)
    meanL = np.mean(l)  # mean of L in HSL
    meanH = np.mean(h)
    meanS = np.mean(s)

    ycbcr = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2YCrCb).reshape((-1, 3))
    y, cr, cb = ycbcr[:, 0].astype(np.float), ycbcr[:, 1].astype(np.float), ycbcr[:, 2].astype(np.float)
    sdCb = np.std(cb)  # SD of Cb in YCbCr
    meanY = np.mean(y)  # mean of L in HSL
    meanCb = np.mean(cb)
    meanCr = np.mean(cr)

    lab = cv2.cvtColor(bgr_vector.reshape((-1, 1, 3)), cv2.COLOR_BGR2LAB).reshape((-1, 3))
    L, a, bu = lab[:, 0].astype(np.float), lab[:, 1].astype(np.float), lab[:, 2].astype(np.float)
    meanA = np.mean(a)  # mean of a in Lab
    meanB = np.mean(bu)  # mean of b in Lab
    meanLa = np.mean(L)
    hue_angle = np.mean(np.arctan(bu/a))


    EXR = 1.4 * Rn - Gn  # excess red
    cive = 0.441 * Rn - 0.811 * Gn + 0.385 * Bn + 18.78  # Color index for extracted vegetation cover(CIVE)
    rbi = (Rn - Bn) / (Rn + Bn)  # Red-blue contrast (RBI)

    writer.writerow([name, EXR, Rn, Gn, Bn, meanL,meanH,meanS,meanLa,meanA, meanB,
                     meanCb,meanCr,meanY, sdCb, cive, rbi, hue_angle, key])
if __name__ == '__main__':
    imgs_path = '/home/nadav/Desktop/svm2/apple data/rgb/'  # TODO
    masks_path = '/home/nadav/Desktop/svm2/apple data/mask/'  # TODO

    images_suffix = '.png'
    masks_suffix = '.png'

    imgs_fnames = [os.path.splitext(x)[0] for x in sorted(os.listdir(imgs_path))]
    masks_fnames = [os.path.splitext(x)[0]  for x in sorted(os.listdir(masks_path))]
    fnames = list(set(imgs_fnames) & set(masks_fnames))

    file = open('/home/nadav/Desktop/svm2/apple data/end_list.csv', 'w', newline='')
    writer = csv.writer(file)
    writer.writerow( ["SN", 'EXR', 'Rn', 'Gn', 'Bn', 'meanL(hsl)', 'meanH(hsl)', 'meanS(hsl)', 'meanLa(lab)', 'meanA(lab)',
         'meanB(lab)', 'meanCb', 'meanCr', 'meanY', 'sdCb', 'cive', 'rbi', 'hue angle', 'grade'])


    count = 1
    for fname in fnames:
        print (fname)
        img_fname = fname + images_suffix
        mask_fname = fname + masks_suffix

        bgr = cv2.imread(os.path.join(imgs_path, img_fname))
        mask = cv2.imread(os.path.join(masks_path, mask_fname))

        # unique_mask_bgr_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)


        bgr_crop = bgr[1:,1:]
        mask_crop = mask[1:,1:]
        key = int(fname[6])
        # this is for cropping rgb only
        # mask_crop = mask_crop[:,:,0]
        # mask_crop[mask_crop > 0] = 1
        # mask_crop[mask_crop == 0] = 0
        # mask_crop = np.expand_dims(mask_crop, 2)
        # mask_crop = np.concatenate((mask_crop, mask_crop, mask_crop), axis=-1)
        # mask_mult_bgr = (bgr_crop * mask_crop).astype('uint8')
        #
        # finame = str(key)+'_'+str(count)
        # if key == 1:
        #     cv2.imwrite('C:/Users/Nadav/Desktop/svm/apple data/only_rgb/1/%s.png' % finame, mask_mult_bgr)
        # if key == 2:
        #     cv2.imwrite('C:/Users/Nadav/Desktop/svm/apple data/only_rgb/2/%s.png' % finame, mask_mult_bgr)
        # if key == 3:
        #     cv2.imwrite('C:/Users/Nadav/Desktop/svm/apple data/only_rgb/3/%s.png' % finame, mask_mult_bgr)
        # if key == 4:
        #     cv2.imwrite('C:/Users/Nadav/Desktop/svm/apple data/only_rgb/4/%s.png' % finame, mask_mult_bgr)

        bgr_3d_vector = bgr_crop[mask_crop[..., 0] != 0]



        # single_fruit_mask = np.all(mask == unq_bgr_val, axis=-1).astype(np.uint8)
        # index_too_keep = (single_fruit_mask == 1)
        # single_fruit_mask = (single_fruit_mask).astype(np.uint8)
        # rgb_masked = bgr[index_too_keep, :]
        # cv2.imwrite('/home/nadav/Desktop/try/', rgb_masked)

        # print(bgr_3d_vector)
        parameters(bgr_3d_vector, fname, key)
        count +=1


