# Import required libraries for performance metrics
import matplotlib.pyplot
from sklearn.metrics import make_scorer
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn.neural_network import MLPClassifier
import numpy as np
import pandas as pd
from IPython.display import display
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
# Define dictionary with performance metrics
pd.set_option('expand_frame_repr', False)
scoring = {'accuracy': make_scorer(accuracy_score),
           'precision': make_scorer(precision_score),
           'recall': make_scorer(recall_score),
           'f1_score': make_scorer(f1_score)}

# Import required libraries for machine learning classifiers
from sklearn.linear_model import LogisticRegression
from sklearn.svm import LinearSVC
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from xgboost import XGBClassifier

# Instantiate the machine learning classifiers
log_model = LogisticRegression(max_iter=10000)
svc_model = LinearSVC(dual=False, C=5)
dtr_model = DecisionTreeClassifier()
rfc_model = RandomForestClassifier(n_estimators=200, min_samples_leaf=10)
gnb_model = GaussianNB()
mlpclassifier = MLPClassifier(hidden_layer_sizes=(15, 20),max_iter=100000, early_stopping=True, n_iter_no_change=50, learning_rate='adaptive',solver = 'adam')
xgbclassifier = XGBClassifier(n_estimators=1000, n_jobs=-1)

# Define the models evaluation function
def models_evaluation(X, y, folds):
    '''
    X : data set features
    y : data set target
    folds : number of cross-validation folds

    '''

    # Perform cross-validation to each machine learning classifier
    seed = 1
    kfold = StratifiedKFold(n_splits=2, shuffle=True, random_state=seed)

    np.random.seed(seed)
    log = cross_validate(log_model, X, y, cv=kfold, scoring=scoring)
    np.random.seed(seed)
    svc = cross_validate(svc_model, X, y, cv=kfold, scoring=scoring)
    np.random.seed(seed)
    dtr = cross_validate(dtr_model, X, y, cv=kfold, scoring=scoring)
    np.random.seed(seed)
    rfc = cross_validate(rfc_model, X, y, cv=kfold, scoring=scoring)
    np.random.seed(seed)
    gnb = cross_validate(gnb_model, X, y, cv=kfold, scoring=scoring)
    np.random.seed(seed)
    mlp = cross_validate(mlpclassifier, X, y, cv=kfold, scoring=scoring)
    np.random.seed(seed)
    xgb = cross_validate(xgbclassifier, X, y, cv=kfold, scoring=scoring)

    # Create a data frame with the models perfoamnce metrics scores
    models_scores_table = pd.DataFrame({'Logistic Regression': [log['test_accuracy'].mean(),
                                                                log['test_precision'].mean(),
                                                                log['test_recall'].mean(),
                                                                log['test_f1_score'].mean()],

                                        'Support Vector Classifier': [svc['test_accuracy'].mean(),
                                                                      svc['test_precision'].mean(),
                                                                      svc['test_recall'].mean(),
                                                                      svc['test_f1_score'].mean()],

                                        'Decision Tree': [dtr['test_accuracy'].mean(),
                                                          dtr['test_precision'].mean(),
                                                          dtr['test_recall'].mean(),
                                                          dtr['test_f1_score'].mean()],

                                        'Random Forest': [rfc['test_accuracy'].mean(),
                                                          rfc['test_precision'].mean(),
                                                          rfc['test_recall'].mean(),
                                                          rfc['test_f1_score'].mean()],

                                        'Gaussian Naive Bayes': [gnb['test_accuracy'].mean(),
                                                                 gnb['test_precision'].mean(),
                                                                 gnb['test_recall'].mean(),
                                                                 gnb['test_f1_score'].mean()],
                                        'MLPClassifier': [mlp['test_accuracy'].mean(),
                                                                 mlp['test_precision'].mean(),
                                                                 mlp['test_recall'].mean(),
                                                                 mlp['test_f1_score'].mean()],
                                        'XGBOOST': [xgb['test_accuracy'].mean(),
                                                          xgb['test_precision'].mean(),
                                                          xgb['test_recall'].mean(),
                                                          xgb['test_f1_score'].mean()]
                                        },

                                       index=['Accuracy', 'Precision', 'Recall', 'F1 Score'])

    # Add 'Best Score' column
    models_scores_table['Best Score'] = models_scores_table.idxmax(axis=1)

    # Return models performance metrics scores data frame
    return (models_scores_table)

if __name__ == '__main__':
    csv_path = '/home/nadav/Desktop/vid_211024_103552_C.csv'
    df_whole = pd.read_csv(csv_path)
    y_data = df_whole[['grade']].values.flatten()
    y_data[y_data == 1] = 4
    y_data[y_data == 2] = 1

    x_data = df_whole.drop(['grade'], 1).values
    x_train, y_train = x_data, y_data
    scaler = StandardScaler()
    x_train = scaler.fit_transform(x_train)

    svc_model.fit(x_data, y_data)


    # Run models_evaluation function
    # .plot(kind='scatter', x='num_children', y='num_pets', color='red')
    models_eval = models_evaluation(x_train, y_train, 5)
    display(models_eval)
    plt.show()