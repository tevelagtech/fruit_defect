import matplotlib.pyplot as plt
import numpy as np
import os
import cv2
import random
import pickle

from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Dropout,Activation,Flatten,Conv2D,MaxPooling2D
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.utils import to_categorical
import time
import tensorflow as tf

DATADIR = '/home/nadav/Desktop/aug crop/'
CATEGORIES = ['healthy','damaged']
training_data = []

# IMG_SIZE = 30

a=cv2.imread('/home/nadav/Desktop/aug crop/healthy/healthy2.png')
def create_traning_data():
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        class_num = np.zeros((2,))
        class_num[CATEGORIES.index(category)] = 1
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path,img))
                # new_array = cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))
                # training_data.append([new_array,class_num])
                training_data.append([img_array, class_num])
            except Exception as e:
                pass

create_traning_data()
random.shuffle(training_data)

X = []
y = []
for featurs,labels in training_data:
    X.append(featurs)
    y.append(labels)
# X = (np.array(X).reshape((-1, 175, 210, 3)))/255.0
X = np.array(X) / 255.
y = np.array(y)
# data AUGMENTATION
#
# import pickle
# pickle_out = open('allx.pickle','wb')
# pickle.dump(X,pickle_out)
# pickle_out.close()
# pickle_out = open('ally.pickle','wb')
# pickle.dump(y,pickle_out)
# pickle_out.close()
# # import tensorflow as tf
#
# X = pickle.load(open('allx.pickle','rb'))
# y = pickle.load(open('ally.pickle','rb'))
# X_test = pickle.load(open('Xdamaged_test.pickle','rb'))
# y_test = pickle.load(open('ydamaged_test.pickle','rb'))

# X_test = X_test/255.0
# dense_layers = [0,1,2]
# layer_sizes = [32,64,128]
# conv_layers = [1,2,3]
dense_layers = [2]
layer_sizes = [128]
conv_layers = [3]

for dense_layer in dense_layers:
    for layer_size in layer_sizes:
        for conv_layer in conv_layers:
            NAME = "{}-conv-{}-nodes-{}-dense_".format(conv_layer,layer_size,dense_layer)
            print (NAME)
            tensorboard = TensorBoard(log_dir='/home/nadav/Desktop/logs/{}'.format(NAME))
            model = Sequential()
            model.add(Conv2D(layer_size,(3,3),input_shape = X.shape[1:]))
            model.add(Activation('relu'))
            model.add(MaxPooling2D(pool_size=(2,2)))
            for l in range(conv_layer-1):
                model.add(Conv2D(layer_size,(3,3)))
                model.add(Activation('relu'))
                model.add(MaxPooling2D(pool_size=(2,2)))
            model.add(Flatten())
            for l in range(dense_layer):
                model.add(Dense(layer_size))
                model.add(Activation('relu'))

            model.add(Dense(2))
            model.add(Activation('softmax'))

            model.compile(loss='categorical_crossentropy',optimizer='adam',metrics=['accuracy'])

            early_stopping_callback = tf.keras.callbacks.EarlyStopping(
                monitor="val_loss",
                patience=6,
            )

            callbacks = [
                tensorboard,
                early_stopping_callback
            ]

            model.fit(X,y,batch_size=50,epochs=30,validation_split=0.10,callbacks=callbacks)
            # model.fit(X, y, batch_size=50, epochs=25, validation_split=0.10)
            # model.save('/home/nadav/Desktop/crop_train/binary/'+NAME)
            model.save('/home/nadav/Desktop/aug/'+NAME, save_format="h5")
            # test_metrics = model.evaluate(X_test, y_test)

            print('-'* 30)
            # print(test_metrics[1])
            print('-'* 30)


