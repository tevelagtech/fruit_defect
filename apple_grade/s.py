import colour
import cv2
import numpy as np
import time
from plantcv import plantcv as pcv
# drone colors red first
import skimage
from pyasn1.compat.octets import null
from sklearn.cross_decomposition import PLSRegression

def unique_count_app(a):
    r, g, b = a[:, 0], a[:, 1], a[:, 2]
    pixels2delete = (r > 200) & (g > 200) & (b > 200)
    pixels2delete = np.bitwise_not(pixels2delete)
    a = a[pixels2delete]
    common = np.mean(a.reshape((-1, 3)), axis=0).astype(np.int)
    # common = np.median(a.reshape((-1, 3)), axis=0).astype(np.int)
    # colors, count = np.unique(a.reshape(-1,a.shape[-1]), axis=0, return_counts=True)
    # return list(colors[count.argmax()])
    return list(common)
bgr = cv2.imread('/home/nadav/Desktop/expo_Color2_Color.png')
rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
mask= cv2.imread('/home/nadav/Desktop/maskexpo.png')
unique_mask_rgb_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)
unique_mask_rgb_vals = unique_mask_rgb_vals[1:]

colors = []
for unq_rgb_val in unique_mask_rgb_vals:
    #     if np.all(unq_rgb_val == 0):
    #         continue

        # print ('after np all',time.time() - start)
        single_fruit_mask = np.all(mask == unq_rgb_val, axis=-1).astype(np.uint8)
        index_too_keep = (single_fruit_mask==1)
        single_fruit_mask = (single_fruit_mask).astype(np.uint8)
        rgb_masked = rgb[index_too_keep,:]
        colors.append(unique_count_app(rgb_masked))
print (colors)
