import numpy as np
import pandas as pd
# from sklearn.ensemble import AdaBoostClassifier, RandomForestClassifier, AdaBoostRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestRegressor
import seaborn as sns
import matplotlib.pyplot as plt
import pickle
import sklearn
print(sklearn.__version__)
from sklearn.neural_network import MLPClassifier, MLPRegressor
from sklearn.svm import LinearSVC
from sklearn.svm import SVC
def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)


if __name__ == '__main__':
    csv_path = '/home/nadav/Desktop/vid_211024_103552_C.csv'
    # test_path = 'C:/Users/Nadav/Desktop/vid_200916_104945_C/vid_200916_104945_C.csv'
    # plot_data = True
    plot_data = False
    split_train_test = False
    if plot_data:
        from sklearn.manifold import TSNE
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D

    df_whole = pd.read_csv(csv_path)
    # df_test = pd.read_csv(test_path)
    y_data = df_whole[['grade']].values.flatten()
    y_data[y_data == 1] = 4
    y_data[y_data == 2] = 1
    # y_data[y_data <= 2] = 0
    # y_data[y_data > 2] = 1
    x_data = df_whole.drop(['grade'], 1).values
    # x_test = df_test.drop(['grade'], 1).values
    # y_test = df_test[['grade']].values.flatten()
    if split_train_test:
        x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=0.10, random_state=42, stratify=y_data)
    else:
        x_train, y_train = x_data, y_data
    # x_train = x_data
    # y_train = y_data
    # clf = MLPClassifier(hidden_layer_sizes=(100,100),max_iter=500,learning_rate='adaptive',solver = 'adam')
    # clf = RandomForestRegressor(n_estimators=1000, max_depth=6, random_state=42)
    # clf = MLPRegressor(hidden_layer_sizes=(15, 30), max_iter = 1000,  learning_rate='adaptive', solver='adam')
    svc_model = LinearSVC(dual=False, C=5)
    clf = svc_model
    scaler = StandardScaler()
    x_train = scaler.fit_transform(x_train)
    if split_train_test:
        x_test = scaler.transform(x_test)

    scl_filename = "/home/nadav/Desktop/scaler.pkl"
    with open(scl_filename, 'wb') as file:
        pickle.dump(scaler, file,protocol=3)

    if plot_data:
        x_data_copy_for_visualization = np.copy(x_data)
        scaler_for_visualization = StandardScaler()
        x_data_copy_for_visualization = scaler_for_visualization.fit_transform(x_data_copy_for_visualization)
        tsne = TSNE(n_components=3)
        X_embedded = tsne.fit_transform(x_data_copy_for_visualization)
        ax = plt.axes(projection='3d')
        ax.scatter(X_embedded[:, 0], X_embedded[:, 1], X_embedded[:, 2], c=y_data, cmap='viridis')
        # ax.scatter(x_data[:, 0], x_data[:, 1], x_data[:, 2], c=y_data, cmap='viridis')
        # ax.set_xlabel('z height')
        # ax.set_ylabel('radius')
        # ax.set_zlabel('color')
        # axisEqual3D(ax)
        plt.show()

    clf.fit(x_train, y_train)
    if split_train_test:
        print(clf.score(x_test,y_test))
        print(np.mean((clf.predict(x_test) - y_test) ** 2) ** 0.5)
        print(np.std(np.abs((clf.predict(x_test) - y_test))))
        y_test_pred = np.clip(np.round(clf.predict(x_test)).astype(np.int64),1,4)
        print('-' * 20)
        print('test stats:')
        # y_test_pred = clf.predict(x_test)
        print(classification_report(y_test, y_test_pred))
        print('accuracy: {}'.format(float(np.sum(y_test == y_test_pred)) / len(y_test)))
        print(np.unique(y_test, return_counts=True))

    print(np.mean((clf.predict(x_train) - y_train) ** 2) ** 0.5)

    y_train_pred = np.round(clf.predict(x_train)).astype(np.int64)

    print ('-'* 20)
    print ('train stats:')
    # y_train_pred = clf.predict(x_train)
    print (classification_report(y_train, y_train_pred))
    print ('accuracy: {}'.format(float(np.sum(y_train==y_train_pred)) / len(x_train)))
    # print (np.unique(x_train, return_counts=True))


    # cm = confusion_matrix(y_test, y_test_pred)
    # f, ax = plt.subplots(figsize = (5,5))
    # sns.heatmap(cm,annot = True, linewidths = 0.5, linecolor = "red", fmt = ".0f",ax = ax)
    # plt.xlabel("y_pred")
    # plt.ylabel("y_true")
    #
    #
    # plt.show()
    pkl_filename = "/home/nadav/Desktop/clf.pkl"
    with open(pkl_filename, 'wb') as file:
        pickle.dump(clf, file,protocol=3)


