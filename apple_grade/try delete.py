import colour
import cv2
import numpy as np
import time
from plantcv import plantcv as pcv
# drone colors red first
import skimage
from pyasn1.compat.octets import null
from sklearn.cross_decomposition import PLSRegression
# red close
ref2 = [[140, 91, 25], [141, 87, 93], [55, 99, 111], [23, 55, 78], [44, 92, 38], [118, 37, 28]]
#red first from first frame

vidref = [[222, 176, 144], [216, 177, 236], [27, 198, 241], [2, 125, 199], [63, 171, 114], [194, 76, 95]]
# red far
ref3 = [[118, 37, 28],[44, 92, 38],[23, 55, 78],[55, 99, 111],[141, 87, 93],[140, 91, 25]]
test = [[114, 85, 80], [190, 140, 131], [79, 125, 159], [99, 115, 98], [114, 136, 175], [99, 158, 163], [190, 113, 77], [55, 107, 156], [180, 86, 97], [86, 76, 112], [150, 157, 98], [214, 144, 97], [44, 89, 138], [76, 119, 84], [149, 66, 73], [204, 152, 86], [163, 81, 113], [18, 130, 162], [196, 190, 195], [168, 165, 169], [143, 140, 147], [110, 109, 118], [81, 83, 91], [57, 58, 61]]
test2 = [[146, 117, 113], [220, 175, 168], [103, 161, 196], [122, 148, 132], [135, 169, 209], [122, 192, 199], [227, 150, 115], [76, 143, 193], [210, 127, 139], [115, 117, 157], [181, 199, 142], [249, 186, 141], [68, 123, 174], [100, 155, 121], [180, 107, 113], [235, 197, 134], [196, 127, 162], [33, 171, 208], [218, 222, 225], [195, 201, 205], [173, 184, 189], [139, 154, 162], [110, 126, 136], [89, 99, 105]]
#vidoe red last
test1 = [[198, 44, 91], [114, 186, 121], [10, 139, 239], [137, 239, 249], [247, 214, 247], [246, 205, 132]]

def unique_count_app(a):
    r, g, b = a[:, 0], a[:, 1], a[:, 2]
    pixels2delete = (r > 200) & (g > 200) & (b > 200)
    pixels2delete = np.bitwise_not(pixels2delete)
    a = a[pixels2delete]
    common = np.mean(a.reshape((-1, 3)), axis=0).astype(np.int)
    # common = np.median(a.reshape((-1, 3)), axis=0).astype(np.int)
    # colors, count = np.unique(a.reshape(-1,a.shape[-1]), axis=0, return_counts=True)
    # return list(colors[count.argmax()])
    return list(common)
bgr = cv2.imread('/home/nadav/Desktop/cal_video.png')
rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
mask= cv2.imread('/home/nadav/Desktop/mask.png')
# mask = cv2.cvtColor(mask1,cv2.COLOR_BGR2GRAY)
cap = cv2.VideoCapture('/home/nadav/Desktop/vid_201103_113338_C.avi')
unique_mask_rgb_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)
unique_mask_rgb_vals = unique_mask_rgb_vals[1:]
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter('/home/nadav/Desktop/vid_201103_113338_C_color.avi',fourcc, 30.0, (640,480))
while(cap.isOpened()):
    ret, frame = cap.read()
    start = time.time()
    colors = []
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    for unq_rgb_val in unique_mask_rgb_vals:
    #     if np.all(unq_rgb_val == 0):
    #         continue

        # print ('after np all',time.time() - start)
        single_fruit_mask = np.all(mask == unq_rgb_val, axis=-1).astype(np.uint8)
        index_too_keep = (single_fruit_mask==1)
        single_fruit_mask = (single_fruit_mask).astype(np.uint8)
        rgb_masked = frame[index_too_keep,:]
        colors.append(unique_count_app(rgb_masked))
    print (colors)
    start = time.time()
    pls = PLSRegression(n_components=3)
    colors4regression = []
    test1_colors4regression = []

    colors_discarded = []
    for index, (rgb_triplet, test1_triplet) in enumerate(zip(colors, vidref)):
        if np.all(np.array(rgb_triplet) > 0):
            colors4regression.append(rgb_triplet)
            test1_colors4regression.append(test1_triplet)


    colors4regression = np.array(colors4regression)
    test1_colors4regression = np.array(test1_colors4regression)
    if len(colors4regression)<3:
        continue
    pls.fit(colors4regression, test1_colors4regression)
    # print('after regression', time.time() - start)
    # print(pls.score(colors,test1))
    #

    #Convert image from int to float
    Float_image=skimage.img_as_float(frame)

    #Normalise image to have pixel values from 0 to 1
    Normalised_image = (Float_image - np.min(Float_image))/np.ptp(Float_image)

    #Decoded the image with sRGB EOTF
    Decoded_img=colour.models.eotf_sRGB(Normalised_image)
    start = time.time()
    #Performed Finlayson 2015 color correction to linear data:
    for im in Decoded_img:
        im[:]=colour.colour_correction(im[:],colors4regression,test1_colors4regression, "Finlayson 2015")
    # print('after correction', time.time() - start)

    #Encoded image back to sRGB
    Encoded_img=colour.models.eotf_inverse_sRGB(Decoded_img)

    #Denormalized image to fit 255 pixel values
    Denormalized_image=Encoded_img*255

    #Converted floats back to integers
    Integer_image= Denormalized_image.astype(int)
    # Integer_image = cv2.cvtColor(Integer_image,cv2.COLOR_BGR2RGB)
    # cv2.imwrite('/home/nadav/Desktop/calsun_video.png', Integer_image)

    display_image = cv2.cvtColor(np.clip(Integer_image, 0, 255).astype(np.uint8), cv2.COLOR_RGB2BGR)
    display_image = cv2.putText(display_image, 'colors used: {}'.format(len(colors4regression)), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 0, 255), 2)

    start_line = 100
    for triplet_found, triplet_real in zip(colors4regression, test1_colors4regression):
        display_image = cv2.circle(display_image, (50, start_line), 1, triplet_found[::-1].astype('uint8').tolist(), 30)
        display_image = cv2.circle(display_image, (100, start_line), 1, triplet_real[::-1].astype('uint8').tolist(), 30)
        start_line += 50
    out.write(display_image)
    cv2.imshow('frame', display_image)
    cv2.waitKey(1)
#     if k == 27:
#         break
# out.release()
cv2.destroyAllWindows()


    # cv2.imwrite('/home/nadav/Desktop/cal_211.png',Integer_image)