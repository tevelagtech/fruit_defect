import cv2
import numpy as np
import os
if __name__ == '__main__':
    imgs_path = '/home/nadav/Desktop/20220301_124045_nactarine_flowers3/color/'
    masks_path = '/home/nadav/Desktop/20220301_124045_nactarine_flowers3/depth'

    images_suffix = '.jpg'
    masks_suffix = '.png'

    imgs_fnames = [x[:x.index(images_suffix)] for x in sorted(os.listdir(imgs_path))]
    masks_fnames = [x[:x.index(masks_suffix)] for x in sorted(os.listdir(masks_path))]
    fnames = list(set(imgs_fnames) & set(masks_fnames))


    for fname in fnames:
        print(fname)
        img_fname = fname + images_suffix
        mask_fname = fname + masks_suffix

        bgr = cv2.imread(os.path.join(imgs_path, img_fname))
        mask = cv2.imread(os.path.join(masks_path, mask_fname),cv2.IMREAD_ANYDEPTH)
        mask[mask == 0] = 1001
        ret, bw_img = cv2.threshold(mask,1000,1, cv2.THRESH_BINARY_INV)
        bw_img_3d = np.dstack((bw_img,bw_img,bw_img))
        c = bgr*bw_img_3d
        kernel = np.ones((4, 4), np.uint8)
        c = cv2.morphologyEx(c.astype(np.uint8), cv2.MORPH_CLOSE, kernel)
        # c[c==(0,0,0)]=135,145,145
        cv2.imshow('',c)
        cv2.waitKey()

