from pyfirmata import Arduino, util
import time
from numpy import arange
import os
import pyrealsense2 as rs
import cv2
import numpy as np
import tensorflow as tf
from tensorflow import keras

class LightsController:
    def __init__(self,PINS):
        self.board = Arduino("COM4")
        self.lightPin = self.board.get_pin("d:3:p")
        self.digital = self.board.digital[9]
        it = util.Iterator(self.board)
        it.start()
        time.sleep(1)
        for pin in PINS:
            self.board.analog[pin].enable_reporting()

    def Off(self):
        self.lightPin.write(0)
        time.sleep(0.1)

    def On(self):
        self.lightPin.write(1)
        time.sleep(0.1)

    def Fade(self):
        for pwm in arange(0.02, 0.1, 0.004):
            self.lightPin.write(pwm)
            time.sleep(0.075)
        for pwm in arange(0.1, 0.8, 0.01):
            self.lightPin.write(pwm)
            time.sleep(0.075)
        for pwm in arange(0.8, 0.1, -0.01):
            self.lightPin.write(pwm)
            time.sleep(0.075)
        for pwm in arange(0.1, 0.02, -0.004):
            self.lightPin.write(pwm)
            time.sleep(0.075)

    def Blink(self):
        self.lightPin.write(1)
        time.sleep(0.1)
        self.lightPin.write(0)
        time.sleep(0.1)
    def Pont(self,pin):

        return self.board.analog[pin].read()

if __name__ == "__main__":
    arduino = LightsController([5])
    model = keras.models.load_model('C:/Users/Nadav/Desktop/apple_detection_dataset/apple_dataset/6calsses_try.h5')
    pipeline = rs.pipeline()
    config = rs.config()
    config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
    pipeline.start(config)
    save_path = '/home/nadav/Desktop/o11.avi'
    sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
    sensor.set_option(rs.option.exposure, 100)
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(save_path, fourcc, 20.0, (640, 480))
    count = 20
    time.sleep(0.1)
    while True:
        frames = pipeline.wait_for_frames(6000000)

        color_frame = frames.get_color_frame()

        X = np.asanyarray(color_frame.get_data())
        crop = X[235:410, 220:430, :] / 255.

        res = model.predict(np.array([crop]))[0]

        CATEGORIES = ['Healthy', 'Moth', 'Pitting', 'Scab', 'Sunburn', 'Undefined']
        # binary
        if res[1] > 0.99:
            score = 'Damaged'
            cv2.rectangle(X, (220, 225), (420, 425), (0, 0, 255), 2)
            cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
            cv2.putText(X, str(round(res[1], 2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
            arduino.On()
        else:
            score = 'Healthy'
            cv2.rectangle(X, (220, 225), (420, 425), (0, 255, 0), 2)
            cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
            cv2.putText(X, str(round(res[0], 2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
            arduino.Off()
        if arduino.Pont(5)<0.8:
            arduino.digital.write(0)
            print ('OK '+str(arduino.Pont(5)))
            arduino.Off()
        elif arduino.Pont(5)<0.9:
            arduino.digital.write(0)
            print('Danger '+str(arduino.Pont(5)))
            arduino.Blink()
            arduino.Off()
        elif arduino.Pont(5)>0.9:
            arduino.digital.write(1)
            arduino.On()
            print('Pump On ' + str(arduino.Pont(5)))

        cv2.imshow('Spirit', X)
        k = cv2.waitKey(1)

        if k == ord('q'):
            break