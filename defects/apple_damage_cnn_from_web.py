from imageai.Detection.Custom import CustomObjectDetection
import os
import pyrealsense2 as rs
import cv2
import numpy as np
execution_path = os.getcwd()

detector = CustomObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath(detection_model_path="/home/nadav/Desktop/aug/3-conv-128-nodes-2-dense_")
detector.setJsonPath(configuration_json="/home/nadav/Desktop/apple_defect cnn/detection_config.json")
detector.loadModel()

# detections = detector.detectObjectsFromImage(input_image="/home/nadav/Desktop/Screenshot from 2021-03-02 09-56-25.png", minimum_percentage_probability=40,
#                                              output_image_path="/home/nadav/Desktop/image-new4.jpg")
#
# for detection in detections:
#     print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
# from imageai.Detection import VideoObjectDetection
# import os
# import cv2
#
# execution_path = os.getcwd()
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
# detector = VideoObjectDetection()
# detector.setModelTypeAsYOLOv3()
# detector.setModelPath("/home/nadav/Desktop/detection_model-ex-028--loss-8.723.h5")
# detector.loadModel()
# # Start streaming
pipeline.start(config)
sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
sensor.set_option(rs.option.exposure, 200)
while True:
    frames = pipeline.wait_for_frames(100000000)

    color_frame = frames.get_color_frame()

    bgr = np.asanyarray(color_frame.get_data())
    bgr2 = bgr
    detections = detector.detectObjectsFromImage(
        input_image=bgr,input_type="array", minimum_percentage_probability=80,
        output_image_path="/home/nadav/Desktop/image-new4.jpg")

    for detection in detections:
        print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
        [x,y,w,h] = detection["box_points"]
        if detection["name"] == 'apple':
            bgr = cv2.rectangle(bgr2, (x, y), (x+(w-x) , y+(h-y) ), (0, 255, 0), 2)
            cv2.putText(bgr2, 'Apple', (x,y-5), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)
        elif detection["name"] == 'damaged_apple':
            bgr = cv2.rectangle(bgr2, (x, y), (x + (w - x), y + (h - y)), (0, 0, 255), 2)
            cv2.putText(bgr2, 'Damaged Apple', (x, y-5), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 0, 255),2)

    cv2.imshow('frame', bgr2)
    cv2.waitKey(1)




    # video_path = detector.detectObjectsFromVideo(camera_input=color_frame,
    #     output_file_path="/home/nadav/Desktop/camera_detected_video"
    #     , frames_per_second=20, log_progress=True, minimum_percentage_probability=20)
    #
    # print(video_path)
