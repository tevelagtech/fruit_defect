import cv2
import numpy as np
from tensorflow import keras
model = keras.models.load_model('/home/nadav/Desktop/6calsses_try.h5')
video_path = '/home/nadav/Desktop/exposureout2.avi'

cap = cv2.VideoCapture(video_path)
save_path = '/home/nadav/Desktop/o13.avi'
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(save_path, fourcc, 20.0, (1280, 480))

while True:
    ret, bgr = cap.read()
    if not ret:
        break

    X = np.asanyarray(bgr)

    visualize_bgr = np.copy(X)
    # crop = X[235:410, 220:430, :] / 255.

    crop = np.copy(X[167:342, 291:501, :]) / 255.
    #apple in center
    #crop = np.copy(X[235:410, 220:430, :]) / 255.
    # seg_crop = np.copy(X[235:410, 220:430, :])
    # cv2.rectangle(visualize_bgr, (291, 167), (501, 342), (255, 255, 255), 2)
    # cv2.rectangle(visualize_bgr, (roi.min_x, roi.min_y), (roi.max_x, roi.max_y), (255, 255, 255))

    # analytic semantic segmentation
    seg_crop = np.copy(X[167:342, 291:501, :])
    b, g, r = cv2.split(seg_crop)
    r_minus_b = np.clip(r.astype(np.int32) - b.astype(np.int), 0, 255).astype(np.uint8)
    ret2, th2 = cv2.threshold(r_minus_b, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    th2 = cv2.morphologyEx(th2, cv2.MORPH_CLOSE, np.ones((5, 5), np.uint8), iterations=4)

    # neural segmentation
    # crop4segmentation = np.copy(X[167:343, 291:507, :])
    # th2 = semantic_segmentation_network.infer_mask(cv2.cvtColor(np.copy(X[167:343, 291:515, :]), cv2.COLOR_BGR2RGB), threshold=segmentation_threshold)[:-1, :-14]
    # th2 = (cv2.morphologyEx(th2.astype(np.uint8)*255, cv2.MORPH_CLOSE, np.ones((11, 11), np.uint8), iterations=4)).astype(np.bool)

    # cv2.imshow('mask', th2)

    crop[np.bitwise_not(
        np.concatenate((np.expand_dims(th2, 2), np.expand_dims(th2, 2), np.expand_dims(th2, 2)), -1).astype(
            np.bool))] = 1.
    cv2.imshow('aaa', (crop * 255).astype(np.uint8))
    res = model.predict(np.array([crop]))[0]

    CATEGORIES = ['Healthy', 'Moth', 'Pitting', 'Scab', 'Sunburn', 'Undefined']
    # binary
    if np.argmax(res) == 0:
        #cv2.rectangle(visualize_bgr, (291, 167), (501, 342), (0, 200, 0), 2)
        cv2.putText(visualize_bgr, str(CATEGORIES[np.argmax(res)]), (291, 153), cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                    (0, 200, 0), 2)
        cv2.putText(visualize_bgr, str(round(max(res) * 100, 2)) + '%', (395, 153), cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                    (0, 200, 0), 2)
        print(str(CATEGORIES[np.argmax(res)]))
    else:
        #cv2.rectangle(visualize_bgr, (291, 167), (501, 342), (0, 0, 200), 2)
        cv2.putText(visualize_bgr, str(CATEGORIES[np.argmax(res)]), (291, 153), cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                    (0, 0, 200), 2)
        cv2.putText(visualize_bgr, str(round(max(res) * 100, 2)) + '%', (395, 153), cv2.FONT_HERSHEY_SIMPLEX, 0.65,
                    (0, 0, 200), 2)
        print(str(CATEGORIES[np.argmax(res)]))

    # if exposure_count % exposure_modulo == 0:
    #     roi_sensor.set_region_of_interest(roi)
    # print(sensor.get_option(rs.option.exposure))
    masked_crop = (crop * 255).astype(np.uint8)
    masked_crop_large = np.zeros_like(visualize_bgr)
    masked_crop_large[167:342, 291:501, :] = masked_crop

    visualize_bgr = np.concatenate((visualize_bgr, masked_crop_large), 1)

    # cv2.putText(visualize_bgr, '{}'.format(sensor.get_option(rs.option.exposure)), (30, 30),
    #             cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0))
    # fps = 1. / (time.time() - start_time)
    # cv2.putText(visualize_bgr, '{}'.format('fps: {}'.format(np.round(fps))), (30, 60), cv2.FONT_HERSHEY_SIMPLEX, 1,
    #             (0, 255, 0))
    out.write(visualize_bgr)
    cv2.imshow('image', visualize_bgr)
    k = cv2.waitKey(100)

    if k == ord('q'):
        break


