import os
import pyrealsense2 as rs
import cv2
import numpy as np
import tensorflow as tf
from tensorflow import keras
model = keras.models.load_model('/home/nadav/Desktop/defects/defects weights/6calsses_try.h5')
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
pipeline.start(config)
save_path = '/home/nadav/Desktop/o11.avi'
sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
sensor.set_option(rs.option.exposure,100)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(save_path, fourcc, 20.0, (640, 480))
count = 20
while True:
    frames = pipeline.wait_for_frames(6000000)

    color_frame = frames.get_color_frame()

    X = np.asanyarray(color_frame.get_data())
    crop = X[235:410, 220:430, :] / 255.

    res = model.predict(np.array([crop]))[0]


    CATEGORIES = ['Healthy', 'Moth', 'Pitting', 'Scab', 'Sunburn', 'Undefined']
    # binary
    if res[1] > 0.85:
        score = 'Damaged'
        cv2.rectangle(X, (220, 225), (420,425), (0,0,255), 2)
        cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
        cv2.putText(X, str(round(res[1],2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
    else:
        score = 'Healthy'
        cv2.rectangle(X, (220, 225), (420,425), (0, 255, 0), 2)
        cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
        cv2.putText(X, str(round(res[0],2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)

    # 6 classes
    # if count>20:
    #     # if X[313,306,1]>200:
    #     #     cv2.rectangle(X, (220, 225), (420, 425), (0, 0, 0), 2)
    #     #     cv2.putText(X, 'No apple', (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 0), 2)
    #     if False:
    #      pass
    #     else:
    #         if np.argmax(res) == 0:
    #             cv2.rectangle(X, (220, 225), (420, 425), (0, 200, 0), 2)
    #             cv2.putText(X, str(CATEGORIES[np.argmax(res)]), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 200, 0), 2)
    #             cv2.putText(X, str(round(max(res)*100,2))+'%', (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 200, 0), 2)
    #             print(str(CATEGORIES[np.argmax(res)]))
    #         else:
    #             cv2.rectangle(X, (220, 225), (420, 425), (0, 0, 200), 2)
    #             cv2.putText(X, str(CATEGORIES[np.argmax(res)]), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 200), 2)
    #             cv2.putText(X, str(round(max(res)*100,2))+'%', (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 200), 2)
    #             print(str(CATEGORIES[np.argmax(res)]))
    out.write(X)
    cv2.imshow('Spirit', X)
    k = cv2.waitKey(1)
    count +=1
    if k == ord('q'):
        break
    # print(score)