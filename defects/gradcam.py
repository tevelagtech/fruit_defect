import pyrealsense2 as rs
import cv2
import numpy as np
import tensorflow as tf
from tensorboard.notebook import display
from tensorflow import keras
model = keras.models.load_model('/home/nadav/Desktop/defects/defects weights/6calsses_try.h5')
# pipeline = rs.pipeline()
# config = rs.config()
# config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
# pipeline.start(config)
# save_path = '/home/nadav/Desktop/ 1rr.avi'
# sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
# sensor.set_option(rs.option.exposure, 210)
# fourcc = cv2.VideoWriter_fourcc(*'XVID')
# out = cv2.VideoWriter(save_path, fourcc, 10.0, (640, 480))
# count = 20

# frames = pipeline.wait_for_frames(100000000)
frames = cv2.imread('/home/nadav/Desktop/moth3722.png')
# color_frame = frames.get_color_frame()


# crop = X[235:410, 220:430, :] / 255.

res = model.predict(np.array([frames]))[0]


CATEGORIES = ['Healthy', 'Moth', 'Pitting', 'Scab', 'Sunburn', 'Undefined']
# binary
# if res[1] > 0.9:
#     score = 'Damaged'
#     cv2.rectangle(X, (220, 225), (420,425), (0,0,255), 2)
#     cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
#     cv2.putText(X, str(round(res[1],2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
# else:
#     score = 'Healthy'
#     cv2.rectangle(X, (220, 225), (420,425), (0, 255, 0), 2)
#     cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
#     cv2.putText(X, str(round(res[0],2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
#
# 6 classes

print(str(CATEGORIES[np.argmax(res)]))
# out.write(X)
# cv2.imshow('Spirit', frames)
# k = cv2.waitKey(1)

import tensorflow as tf
import cv2
import numpy as np


def get_saliency_map(model, image, class_idx):
    image = tf.convert_to_tensor(image)
    with tf.GradientTape() as tape:
        tape.watch(image)
        predictions = model(image)

        loss = predictions[:, class_idx]

    # Get the gradients of the loss w.r.t to the input image.
    gradient = tape.gradient(loss, image)

    # take maximum across channels
    gradient = tf.reduce_max(gradient, axis=-1)

    # convert to numpy
    gradient = gradient.numpy()

    # normaliz between 0 and 1
    min_val, max_val = np.min(gradient), np.max(gradient)
    smap = (gradient - min_val) / (max_val - min_val + tf.keras.backend.epsilon())

    return smap


if __name__ == '__main__':
    bgr = cv2.imread('/home/nadav/Desktop/moth3722.png')
    # bgr = cv2.imread('/home/yotam/docker_images/docker_shared/defects/sun burn890.png')
    model = tf.keras.models.load_model('/home/nadav/Desktop/defects/defects weights/6calsses_try.h5')
    conv_name = 'conv5_block3_3_conv'
    im2predict = np.expand_dims(bgr, 0) / 255.
    prediction = model.predict(im2predict)[0]
    pred_idx = np.argmax(prediction)
    pred_prob = prediction[pred_idx]

    smap = get_saliency_map(model, im2predict, pred_idx)

    smap = (smap[0] * 255).astype(np.uint8)
    blur = cv2.GaussianBlur(smap, (5, 5), 0)
    ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    kernel = np.ones((5, 5), np.uint8)
    closing = cv2.morphologyEx(th3, cv2.MORPH_CLOSE, kernel)

    cv2.namedWindow('segmentation', cv2.WND_PROP_FULLSCREEN)
    cv2.imshow('segmentation', closing)
    cv2.waitKey(1)

    cv2.namedWindow('smap', cv2.WND_PROP_FULLSCREEN)
    cv2.imshow('smap', smap)
    cv2.waitKey(1)


    #
    cv2.putText(bgr, '{}:{}'.format(pred_idx, str(np.round(pred_prob, 3))), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0))
    cv2.namedWindow('image', cv2.WND_PROP_FULLSCREEN)
    cv2.imshow('image', bgr)
    cv2.waitKey(0)