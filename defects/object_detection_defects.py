import os
import pyrealsense2 as rs
import cv2
import numpy as np
import tensorflow as tf
from tensorflow import keras
from imageai.Detection.Custom import CustomObjectDetection

execution_path = os.getcwd()

detector = CustomObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath(detection_model_path="/home/nadav/Desktop/apple_defect cnn/detection_model-ex-028--loss-8.723.h5")
detector.setJsonPath(configuration_json="/home/nadav/Desktop/apple_defect cnn/detection_config.json")
detector.loadModel()
model = keras.models.load_model('/home/nadav/Desktop/defects/defects weights/6calsses_try.h5')
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)
pipeline.start(config)
save_path = '/home/nadav/Desktop/ g1rr.avi'
sensor = pipeline.get_active_profile().get_device().query_sensors()[1]
sensor.set_option(rs.option.exposure, 150)
fourcc = cv2.VideoWriter_fourcc(*'XVID')
out = cv2.VideoWriter(save_path, fourcc, 10.0, (640, 480))
CATEGORIES = ['Healthy', 'Moth', 'Pitting', 'Scab', 'Sunburn', 'Undefined']
def get_saliency_map(model, image, class_idx):
    image = tf.convert_to_tensor(image)
    with tf.GradientTape() as tape:
        tape.watch(image)
        predictions = model(image)

        loss = predictions[:, class_idx]

    # Get the gradients of the loss w.r.t to the input image.
    gradient = tape.gradient(loss, image)

    # take maximum across channels
    gradient = tf.reduce_max(gradient, axis=-1)

    # convert to numpy
    gradient = gradient.numpy()

    # normaliz between 0 and 1
    min_val, max_val = np.min(gradient), np.max(gradient)
    smap = (gradient - min_val) / (max_val - min_val + tf.keras.backend.epsilon())

    return smap
while True:
    frames = pipeline.wait_for_frames(100000000)

    color_frame = frames.get_color_frame()

    X = np.asanyarray(color_frame.get_data())

    visualize_bgr = np.copy(X)
    # crop = X[235:410, 220:430, :] / 255.

    detections = detector.detectObjectsFromImage(
        input_image=X, input_type="array", minimum_percentage_probability=80,
        output_image_path="/home/nadav/Desktop/image-new4.jpg")
    for detection in detections:
        [x, y, w, h] = detection["box_points"]
        crop1 = X[y:h, x:w , :] / 255.

        cv2.imshow('crop', (crop1*255).astype(np.uint8))
        cv2.waitKey(1)
        try:
            crop1 = cv2.resize(crop1,(210,175))
        except:
            pass
    # for detection in detections:
    #     print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
    #     [x, y, w, h] = detection["box_points"]
    #     if detection["name"] == 'apple':
    #         bgr = cv2.rectangle(X, (x, y), (x + (w - x), y + (h - y)), (0, 255, 0), 2)
    #         cv2.putText(X, 'Apple', (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 255, 0), 2)
    #     elif detection["name"] == 'damaged_apple':
    #         bgr = cv2.rectangle(X, (x, y), (x + (w - x), y + (h - y)), (0, 0, 255), 2)
    #         cv2.putText(X, 'Damaged Apple', (x, y - 5), cv2.FONT_HERSHEY_SIMPLEX, 0.55, (0, 0, 255), 2)

        res = model.predict(np.array([crop1]))[0]
    # binary
    # if res[1] > 0.9:
    #     score = 'Damaged'
    #     cv2.rectangle(X, (220, 225), (420,425), (0,0,255), 2)
    #     cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
    #     cv2.putText(X, str(round(res[1],2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 2)
    # else:
    #     score = 'Healthy'
    #     cv2.rectangle(X, (220, 225), (420,425), (0, 255, 0), 2)
    #     cv2.putText(X, str(score), (220, 190), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
    #     cv2.putText(X, str(round(res[0],2)), (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 2)
    #
    # 6 classes

    # if X[313,306,1]>200:
    #     cv2.rectangle(X, (220, 225), (420, 425), (0, 0, 0), 2)
    #     cv2.putText(X, 'No apple', (220, 220), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 0), 2)


        if np.max(res) > 0.5 and np.argmax(res)!= 0:
            cv2.putText(visualize_bgr, str('Damaged'),(x, y-30), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 1)
            cv2.putText(visualize_bgr, str(round(max(res)*100,2))+'%', (x, y-10), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 0, 255), 1)
            cv2.rectangle(visualize_bgr, (x, y), (x + (w - x), y + (h - y)), (0, 0, 255), 1)
        else:
            cv2.putText(visualize_bgr, str(CATEGORIES[0]), (x, y - 30), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (0, 255, 0), 1)
            cv2.rectangle(visualize_bgr, (x, y), (x + (w - x), y + (h - y)), (0, 255, 0), 1)
        print(str(CATEGORIES[np.argmax(res)]))


        conv_name = 'conv5_block3_3_conv'
        im2predict = np.expand_dims(visualize_bgr, 0) / 255.
        prediction = model.predict(np.array([crop1]))[0]
        pred_idx = np.argmax(prediction)
        pred_prob = prediction[pred_idx]

        smap = get_saliency_map(model, np.array([crop1]), pred_idx)

        smap = (smap[0] * 255).astype(np.uint8)
        blur = cv2.GaussianBlur(smap, (5, 5), 0)
        ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        kernel = np.ones((5, 5), np.uint8)
        closing = cv2.morphologyEx(th3, cv2.MORPH_CLOSE, kernel)

        cv2.namedWindow('segmentation', cv2.WND_PROP_FULLSCREEN)
        cv2.imshow('segmentation', closing)
        cv2.waitKey(1)

        cv2.namedWindow('smap', cv2.WND_PROP_FULLSCREEN)
        cv2.imshow('smap', smap)
        cv2.waitKey(1)

        #
        # cv2.putText(visualize_bgr, '{}:{}'.format(pred_idx, str(np.round(pred_prob, 3))), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 1,
        #             (0, 255, 0))
        cv2.namedWindow('image', cv2.WND_PROP_FULLSCREEN)
        cv2.imshow('image', visualize_bgr)
        k = cv2.waitKey(1)
        if k == ord('q'):
            break
    # print(score)