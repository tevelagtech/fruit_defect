import matplotlib.pyplot as plt
import numpy as np
import os
import cv2
import random
import pickle

from keras_preprocessing.image import ImageDataGenerator
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense,Dropout,Activation,Flatten,Conv2D,MaxPooling2D
from tensorflow.keras.callbacks import TensorBoard
from tensorflow.keras.losses import CategoricalCrossentropy
from tensorflow.keras.utils import to_categorical
import time
import tensorflow as tf
from tensorflow import keras
DATADIR = '/home/nadav/Desktop/all/'
# CATEGORIES = ['healthy','moth','pitting','scab','sun burn','undifiend']
CATEGORIES = ['healthy','damaged']
# data = ['6classall','kirsner_crop','old_again_crop']
data = ['healthy','damaged']
training_data = []

# IMG_SIZE = 30


def create_traning_data():
    # #6 clases
    # for datadir in data:
    #     directory = os.path.join(DATADIR, datadir)
    #     for category in CATEGORIES:
    #         path = os.path.join(directory, category)
    #         class_num = np.zeros((6,))
    #         class_num[CATEGORIES.index(category)] = 1
    #         for img in os.listdir(path):
    #             try:
    #                 img_array = cv2.imread(os.path.join(path,img))
    #                 # new_array = cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))
    #                 # training_data.append([new_array,class_num])
    #                 training_data.append([img_array, class_num])
    #             except Exception as e:
    #                 pass
    #two clases
    for category in CATEGORIES:
        path = os.path.join(DATADIR, category)
        class_num = np.zeros((2,))
        class_num[CATEGORIES.index(category)] = 1
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path, img))
                # new_array = cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))
                # training_data.append([new_array,class_num])
                training_data.append([img_array, class_num])
            except Exception as e:
                pass
create_traning_data()
random.shuffle(training_data)

X = []
y = []
for featurs,labels in training_data:
    X.append(featurs)
    y.append(labels)
X = np.array(X)
y = np.array(y)
# data AUGMENTATION
train_size = 0.9

x_train = X[:int(len(X) * train_size)]
x_val = X[int(len(X) * train_size):] / 255.
y_train = y[:int(len(y) * train_size)]
y_val = y[int(len(y) * train_size):]
aug = ImageDataGenerator(
    rotation_range=8,
    zoom_range=0.10,
    brightness_range= [0.6,1.0],
	width_shift_range=0.2,
    height_shift_range=0.2,
    shear_range=0.01,
	horizontal_flip=True,
    fill_mode="nearest",
    rescale=1./255.)


# model = keras.models.load_model('/home/nadav/Desktop/crop_train/binary/3-conv-128-nodes-2-dense_/')
# model_input = tf.keras.layers.Input((175, 210, 3))
# pretrained_backbone = tf.keras.applications.ResNet50(
#                 include_top=False,
#                 weights="imagenet",
#                 input_tensor=None,
#                 input_shape=None,
#                 pooling=None,
#                 classes=2)
#
# # pretrained_backbone = tf.keras.applications.MobileNetV2(
# #     include_top=False,
# #     weights="imagenet",
# #     input_tensor=None,
# #     input_shape=None,
# #     pooling=None,
# #     classes=6
# # )
#
# pretrained_output = pretrained_backbone(model_input)
# # flatten = tf.keras.layers.Flatten()(pretrained_output)  # TODO old
# flatten = tf.keras.layers.GlobalMaxPooling2D()(pretrained_output)  # TODO new
# flatten = tf.keras.layers.Dense(128, activation='relu')(flatten)  # TODO new
#
# # fc = tf.keras.layers.Dense(128, activation='relu')(flatten)
# model_output = tf.keras.layers.Dense(2, activation='softmax')(flatten)
#
# model = tf.keras.models.Model(model_input, model_output)

model = tf.keras.models.Sequential([
    tf.keras.applications.ResNet50(
                    include_top=False,
                    weights="imagenet",
                    input_tensor=None,
                    input_shape=(175, 210, 3),
                    pooling=None,
                    classes=2),
    tf.keras.layers.GlobalMaxPooling2D(),
    tf.keras.layers.Dense(128, activation='relu'),
    tf.keras.layers.Dense(2, activation='softmax')


])

# model.summary()
lr_callback = tf.keras.callbacks.ReduceLROnPlateau(
    monitor="val_loss",
    factor=0.1,
    patience=30,
    verbose=0,
    mode="auto",
    min_delta=0.0001,
    cooldown=0,
    min_lr=0,
)
early_stopping_callback = tf.keras.callbacks.EarlyStopping(
                monitor="val_loss",
                patience=100,
                mode='min',
                min_delta=0
)

callbacks = [lr_callback, early_stopping_callback]

model.compile(loss='binary_crossentropy', optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4), metrics=['accuracy',])
model.fit_generator(aug.flow(x_train, y_train, batch_size=50),
                    validation_data=(x_val, y_val),
                    steps_per_epoch=len(x_train) // 50,
                    epochs=300,
                    callbacks=callbacks)
# model.compile(loss='categorical_crossentropy', optimizer=tf.keras.optimizers.Adam(learning_rate=1e-4), metrics=['accuracy',])
# model.fit(x_train/255., y_train,batch_size=50,epochs=30,validation_split=0.10,callbacks=callbacks)
model.save('/home/nadav/Desktop/all/2calsses_resnet.h5' ,save_format="h5")
# # X = pickle.load(open('Xdamaged_test.pickle','rb'))
# y = pickle.load(open('ydamaged_test.pickle','rb'))
# model = keras.models.load_model('/binvester/Tensor_flow image/records/6calsses_resnet.h5')
# X = X/255.0
# #
# test_metrics = model.evaluate(X, y)

# y_pred = model.predict(X)
# y_real = np.argmax(y, axis=1)
# error_predictions_indices = np.argwhere(np.argmax(y_pred, axis=1) != y_real).flatten()
# for idx in error_predictions_indices:
#     model_proba = np.max(y_pred[idx])
#     model_label = np.argmax(y_pred[idx])
#     real = y_real[idx]

#     bgr_cpy = np.copy(X[idx] * 255).astype(np.uint8)
#     bgr_cpy = cv2.resize(bgr_cpy, (512, 512))
#     cv2.putText(bgr_cpy, 'real label: {}'.format(np.round(real)), (30, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 1)
#     cv2.putText(bgr_cpy, 'model label: {}'.format(np.round(model_label)), (30, 60), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 1)
#     cv2.putText(bgr_cpy, 'model probability: {}'.format(np.round(model_proba, 2)), (30, 90), cv2.FONT_HERSHEY_SIMPLEX, 1, (0,0,255), 1)
#     # plt.imshow(cv2.cvtColor((X[idx] * 255).astype(np.uint8), cv2.COLOR_BGR2RGB))
#     cv2.imshow('', bgr_cpy)
#     cv2.waitKey(0)

# print('-'* 30)
# print(test_metrics[1])
# print('-'* 30)
