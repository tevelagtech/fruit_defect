import cv2


idx = 4
cap = cv2.VideoCapture(idx)


fourcc = cv2.VideoWriter_fourcc(*'mp4v')
# fourcc = cv2.VideoWriter_fourcc(*'X264')
# out = cv2.VideoWriter('testing.avi', *'X264', 30, (640, 480))
out = cv2.VideoWriter('/home/nadav/Desktop/outdoor_sun.avi', fourcc, 15, (640, 480))

ret, bgr = cap.read()

while ret:

    cv2.imshow('', bgr)
    k = cv2.waitKey(1)
    ret, bgr = cap.read()

    out.write(bgr)

    if k == ord('q'):
        break


