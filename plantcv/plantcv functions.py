# Import Libraries
from plantcv import plantcv as pcv
import matplotlib
matplotlib.use('TKAgg')


class options:
    def __init__(self):
        self.image = "C:/users/nadav/Desktop/nadav_example_9.png"
        self.debug = "plot"
        self.writeimg = False
        self.result = "./vis_tutorial_results"
        self.outdir = "."  # Store the output to the current directory


# Get options
args = options()

# Set debug to the global parameter
pcv.params.debug = args.debug
# Read image

# Inputs:
#   filename - Image file to be read in
#   mode - How to read in the image; either 'native' (default), 'rgb', 'gray', or 'csv'
img, path, filename = pcv.readimage(filename=args.image,mode='rgb')
# Convert RGB to HSV and extract the saturation channel

# Inputs:
#   rgb_image - RGB image data
#   channel - Split by 'h' (hue), 's' (saturation), or 'v' (value) channel
s = pcv.rgb2gray_hsv(rgb_img=img, channel='s')
# Take a binary threshold to separate plant from background.
# Threshold can be on either light or dark objects in the image.

# Inputs:
#   gray_img - Grayscale image data
#   threshold- Threshold value (between 0-255)
#   max_value - Value to apply above threshold (255 = white)
#   object_type - 'light' (default) or 'dark'. If the object is lighter than
#                 the background then standard threshold is done. If the object
#                 is darker than the background then inverse thresholding is done.
s_thresh = pcv.threshold.binary(gray_img=s, threshold=130, max_value=255, object_type='light')

# Median Blur
# Inputs:
#   gray_img - Grayscale image data
#   ksize - Kernel size (integer or tuple), (ksize, ksize) box if integer input,
#           (n, m) box if tuple input
s_mblur = pcv.median_blur(gray_img=s_thresh, ksize=5)
s_cnt = pcv.median_blur(gray_img=s_thresh, ksize=5)
# Convert RGB to LAB and extract the blue channel ('a')
# Input:
#   rgb_img - RGB image data
#   channel- Split by 'l' (lightness), 'a' (green-magenta), or 'b' (blue-yellow) channel
a = pcv.rgb2gray_lab(rgb_img=img, channel='a')

# Threshold the blue channel image
a_thresh = pcv.threshold.binary(gray_img = a, threshold=140, max_value=255,object_type='light')
a_cnt = pcv.threshold.binary(gray_img=a, threshold=140, max_value=255, object_type='light')

# Fill small objects (optional)
fill = pcv.fill(a_thresh, 30)

#Join the thresholded saturation and blue-yellow images

# Inputs:
#   bin_img1 - Binary image data to be compared to bin_img2
#   bin_img2 - Binary image data to be compared to bin_img1
bs = pcv.logical_or(bin_img1=s_mblur, bin_img2=a_cnt)
# Apply Mask (for VIS images, mask_color=white)
# Inputs:
#   rgb_img - RGB image data
#   mask - Binary mask image data
#   mask_color - 'white' or 'black'
masked = pcv.apply_mask(img=img, mask=bs, mask_color='white')
# Convert RGB to LAB and extract the Green-Magenta and Blue-Yellow channels
masked_a = pcv.rgb2gray_lab(rgb_img=masked, channel='a')
masked_b = pcv.rgb2gray_lab(rgb_img=masked, channel='b')

# Threshold the green-magenta and blue images
maskeda_thresh = pcv.threshold.binary(gray_img=masked_a, threshold=115,
                                      max_value=255, object_type='dark')
maskeda_thresh1 = pcv.threshold.binary(gray_img=masked_a, threshold=135,
                                       max_value=255, object_type='light')
maskedb_thresh = pcv.threshold.binary(gray_img=masked_b, threshold=128,
                                      max_value=255, object_type='light')

# Join the thresholded saturation and blue-yellow images (OR)
ab1 = pcv.logical_or(bin_img1=maskeda_thresh, bin_img2=maskedb_thresh)
ab = pcv.logical_or(bin_img1=maskeda_thresh1, bin_img2=ab1)

# Fill small objects
# Inputs:
#   bin_img - Binary image data
#   size - Minimum object area size in pixels (must be an integer), and smaller objects will be filled
ab_fill = pcv.fill(bin_img=ab, size=200)

# Apply mask (for VIS images, mask_color=white)
masked2 = pcv.apply_mask(img=masked, mask=ab_fill, mask_color='white')
# Define ROI
# Identify objects
id_objects, obj_hierarchy = pcv.find_objects(masked2, ab_fill)
# Inputs:
#   img - RGB or grayscale image to plot the ROI on
#   x - The x-coordinate of the upper left corner of the rectangle
#   y - The y-coordinate of the upper left corner of the rectangle
#   h - The height of the rectangle
#   w - The width of the rectangle
roi1, roi_hierarchy = pcv.roi.rectangle(img=masked2, x=100, y=100, h=200, w=200)






