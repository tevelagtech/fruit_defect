#!/usr/bin/env python
import os
import argparse
from plantcv import plantcv as pcv
import cv2


#### Start of the Main/Customizable portion of the workflow.

### Main workflow
def main():
    # Get options


    # Read image
    img, path, filename = pcv.readimage(filename='/home/nadav/Desktop/vlcsnap-2021-04-27-10h30m54s078.png')
    cv2.imshow('h',img)
    cv2.waitKey(0)
    # Convert RGB to HSV and extract the saturation channel
    s = pcv.rgb2gray_hsv(rgb_img=img, channel='s')
    pcv.rgb2gray_lab(

    )
    cv2.imshow('h', s)
    cv2.waitKey(0)
    # Threshold the saturation image
    s_thresh = pcv.threshold.binary(gray_img=s, threshold=185, max_value=255, object_type='light')
    cv2.imshow('h', s_thresh)
    cv2.waitKey(0)
    # Median Blur
    s_mblur = pcv.median_blur(gray_img=s_thresh, ksize=5)
    s_cnt = pcv.median_blur(gray_img=s_thresh, ksize=5)
    cv2.imshow('h', s_mblur)
    cv2.waitKey(0)
    # Convert RGB to LAB and extract the Blue channel
    l = pcv.rgb2gray_lab(rgb_img=img, channel='L')
    cv2.imshow('l', l)
    cv2.waitKey(0)
    # Threshold the blue image
    b_thresh = pcv.threshold.binary(gray_img=l, threshold=90 , max_value=100, object_type='light')
    cv2.imshow('l', b_thresh)
    cv2.waitKey(0)
    b_cnt = pcv.threshold.binary(gray_img=b_thresh, threshold=160, max_value=255, object_type='light')

    # Fill small objects
    # b_fill = pcv.fill(b_thresh, 10)

    # Join the thresholded saturation and blue-yellow images
    bs = pcv.logical_or(bin_img1=s_mblur, bin_img2=b_cnt)

    # Apply Mask (for VIS images, mask_color=white)
    masked = pcv.apply_mask(img=img, mask=bs, mask_color='white')
    cv2.imshow('h', masked)
    cv2.waitKey(0)    # Convert RGB to LAB and extract the Green-Magenta and Blue-Yellow channels
    masked_a = pcv.rgb2gray_lab(rgb_img=masked, channel='a')
    masked_b = pcv.rgb2gray_lab(rgb_img=masked, channel='b')

    # Threshold the green-magenta and blue images
    maskeda_thresh = pcv.threshold.binary(gray_img=masked_a, threshold=115, max_value=255, object_type='dark')
    maskeda_thresh1 = pcv.threshold.binary(gray_img=masked_a, threshold=135, max_value=255, object_type='light')
    maskedb_thresh = pcv.threshold.binary(gray_img=masked_b, threshold=128, max_value=255, object_type='light')

    # Join the thresholded saturation and blue-yellow images (OR)
    ab1 = pcv.logical_or(bin_img1=maskeda_thresh, bin_img2=maskedb_thresh)
    ab = pcv.logical_or(bin_img1=maskeda_thresh1, bin_img2=ab1)

    # Fill small objects
    ab_fill = pcv.fill(bin_img=ab, size=200)

    # Apply mask (for VIS images, mask_color=white)
    masked2 = pcv.apply_mask(img=masked, mask=ab_fill, mask_color='white')

    # Identify objects
    id_objects, obj_hierarchy = pcv.find_objects(img=masked2, mask=ab_fill)

    # Define ROI
    roi1, roi_hierarchy= pcv.roi.rectangle(img=masked2, x=100, y=100, h=200, w=200)

    # Decide which objects to keep
    roi_objects, hierarchy3, kept_mask, obj_area = pcv.roi_objects(img=img, roi_contour=roi1,
                                                               roi_hierarchy=roi_hierarchy,
                                                               object_contour=id_objects,
                                                               obj_hierarchy=obj_hierarchy,
                                                               roi_type='partial')

    # Object combine kept objects
    obj, mask = pcv.object_composition(img=img, contours=roi_objects, hierarchy=hierarchy3)

    ############### Analysis ################

    outfile = False


    # Find shape properties, output shape image (optional)
    shape_imgs = pcv.analyze_object(img=img, obj=obj, mask=mask, label="default")

    # Shape properties relative to user boundary line (optional)
    boundary_img1 = pcv.analyze_bound_horizontal(img=img, obj=obj, mask=mask, line_position=1680, label="default")

    # Determine color properties: Histograms, Color Slices, output color analyzed histogram (optional)
    color_histogram = pcv.analyze_color(rgb_img=img, mask=mask, hist_plot_type='all', label="default")

    # Pseudocolor the grayscale image
    pseudocolored_img = pcv.visualize.pseudocolor(gray_img=s, mask=mask, cmap='jet')
    cv2.imshow('h', pseudocolored_img)
    cv2.waitKey(0)
    # Write shape and color data to results file


if __name__ == '__main__':
    main()