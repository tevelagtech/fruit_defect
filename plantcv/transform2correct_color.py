# # from plantcv import plantcv as pcv
# import matplotlib
# matplotlib.use('TKAgg')
# img, path, filename = pcv.readimage("C:/Users/Nadav/Desktop/3h_Color.png")
# #
# mask = pcv.transform.create_color_card_mask(rgb_img=img, radius=10, start_coord=(200,200), spacing=(10,10), ncols=1, nrows=3)
# # pcv.plot_image(mask)
# from plantcv import plantcv as pcv
# import cv2
# import numpy as np
# import matplotlib
# matplotlib.use('TKAgg')
# pcv.params.debug = "plot"
#
# img, imgpath, imgname = pcv.readimage(filename="C:/Users/Nadav/Desktop/New folder/nadav_example_17.png") #read in img
# pcv.plot_image(img)
# #Using the pixel coordinate on the plotted image, designate a region of interest for an n x n pixel region in each color chip.
#
# dimensions = [15, 15]  #pixel ROI dimensions
#
# chips = []
# #Declare first row:
#
# # Inputs:
# #   img - RGB or grayscale image to plot the ROI on
# #   x - The x-coordinate of the upper left corner of the rectangle
# #   y - The y-coordinate of the upper left corner of the rectangle
# #   h - The height of the rectangle
# #   w - The width of the rectangle
# chips.append(pcv.roi.rectangle(img=img, x=290, y=320, w=dimensions[0], h=dimensions[1]),) #blue
# chips.append(pcv.roi.rectangle(img=img, x=270 , y=336 , w=dimensions[0], h=dimensions[1]))#red
# chips.append(pcv.roi.rectangle(img=img, x=280 , y=350 , w=dimensions[0], h=dimensions[1]))#black
#
# mask = np.zeros(shape=np.shape(img)[:2], dtype = np.uint8()) # create empty mask img.
#
# print(mask)
#
# # draw contours for each region of interest and give them unique color values.
#
# i = 1
# for chip in chips:
#     print(chip)
#     mask = cv2.drawContours(mask, chip[0], -1, (i*10), -1)
#     i += 1
#
#
# pcv.plot_image(mask, cmap="gray")
#
# mask = mask * 10  #multiply values in the mask for greater contrast. Exclude if designating have more than 25 color chips.
#
# np.unique(mask)
# pcv.print_image(img=mask, filename="test_mask2.png") #write to file.

from plantcv import plantcv as pcv
import cv2
import numpy as np
import matplotlib
matplotlib.use('TKAgg')
pcv.params.debug = "plot" #set debug mode

target_img, targetpath, targetname = pcv.readimage(filename="C:/Users/Nadav/Desktop/New folder/3h_Color.png")
source_img, sourcepath, sourcename = pcv.readimage(filename="C:/Users/Nadav/Desktop/New folder/nadav_example_17.png")
mask, maskpath, maskname = pcv.readimage(filename="C:/Users/Nadav/Desktop/New folder/test_mask.png")
mask1, maskpath1, maskname1 = pcv.readimage(filename="C:/Users/Nadav/Desktop/New folder/test_mask2.png")
#Since target_img and source_img have the same zoom and colorchecker position,
# the same mask can be used for both.
#.npz files containing target_matrix, source_matrix, and
# transformation_matrix will be saved to the output_directory file path

output_directory = "C:/Users/Nadav/Desktop/New folder/test1"
# Inputs:
#   target_img - RGB image with color chips
#   target_mask - Grayscale image with color chips and background each represented with unique values
#   source_img - RGB image with color chips
#   source_mask - Grayscale image with color chips and background each represented with unique values
#   output_directory - File path to which the target_matrix, source_matrix, and tranformation_matrix will be saved
target_matrix, source_matrix, transformation_matrix, corrected_img = pcv.transform.correct_color(target_img=target_img,
                                                                                                 target_mask=mask,
                                                                                                 source_img=source_img,
                                                                                                 source_mask=mask1,
                                                                                                 output_directory=output_directory)
