from plantcv import plantcv as pcv
import matplotlib
import cv2
import numpy as np
matplotlib.use('TKAgg')
img, imgpath, imgname = pcv.readimage(filename="/home/nadav/Pictures/redl_Color.png")
pcv.plot_image(img)
mask = pcv.transform.create_color_card_mask(rgb_img=img, radius=20, start_coord=(110,74), spacing=(64,54), ncols=6, nrows=4)

masked_img = cv2.addWeighted(img, 1, np.stack([mask, mask, mask]).transpose(1, 2, 0), 0.9, -1)
pcv.plot_image(masked_img)
pcv.plot_image(mask)

#
# pcv.params.debug = "plot"
#
#
# pcv.plot_image(img)
# # #Using the pixel coordinate on the plotted image, designate a region of interest for an n x n pixel region in each color chip.
# #
# dimensions = [60, 40]  #pixel ROI dimensions
# #
# chips = []
# # #Declare first row:
# #
# # # Inputs:
# # #   img - RGB or grayscale image to plot the ROI on
# # #   x - The x-coordinate of the upper left corner of the rectangle
# # #   y - The y-coordinate of the upper left corner of the rectangle
# # #   h - The height of the rectangle
# # #   w - The width of the rectangle
# chips.append(pcv.roi.rectangle(img=img, x=90, y=50, w=dimensions[0], h=dimensions[1]),) #red
# chips.append(pcv.roi.rectangle(img=img, x=90 , y=120 , w=dimensions[0], h=dimensions[1]))#green
# chips.append(pcv.roi.rectangle(img=img, x=90 , y=170 , w=dimensions[0], h=dimensions[1]))#blue
# chips.append(pcv.roi.rectangle(img=img, x=90 , y=220 , w=dimensions[0], h=dimensions[1]))#turq
# chips.append(pcv.roi.rectangle(img=img, x=155 , y=50 , w=dimensions[0], h=dimensions[1]))#pink
# chips.append(pcv.roi.rectangle(img=img, x=155 , y=120 , w=dimensions[0], h=dimensions[1]))#orange
# chips.append(pcv.roi.rectangle(img=img, x=155, y=170, w=dimensions[0], h=dimensions[1]),) #red
# chips.append(pcv.roi.rectangle(img=img, x=155 , y=220 , w=dimensions[0], h=dimensions[1]))#green
# chips.append(pcv.roi.rectangle(img=img, x=210 , y=50 , w=dimensions[0], h=dimensions[1]))#blue
# chips.append(pcv.roi.rectangle(img=img, x=210 , y=120 , w=dimensions[0], h=dimensions[1]))#turq
# chips.append(pcv.roi.rectangle(img=img, x=210 , y=170 , w=dimensions[0], h=dimensions[1]))#pink
# chips.append(pcv.roi.rectangle(img=img, x=210 , y=220 , w=dimensions[0], h=dimensions[1]))#orange
# chips.append(pcv.roi.rectangle(img=img, x=265, y=50, w=dimensions[0], h=dimensions[1]),) #red
# chips.append(pcv.roi.rectangle(img=img, x=265 , y=120 , w=dimensions[0], h=dimensions[1]))#green
# chips.append(pcv.roi.rectangle(img=img, x=265 , y=170 , w=dimensions[0], h=dimensions[1]))#blue
# chips.append(pcv.roi.rectangle(img=img, x=265 , y=220 , w=dimensions[0], h=dimensions[1]))#turq
# chips.append(pcv.roi.rectangle(img=img, x=320 , y=50 , w=dimensions[0], h=dimensions[1]))#pink
# chips.append(pcv.roi.rectangle(img=img, x=320 , y=120 , w=dimensions[0], h=dimensions[1]))#orange
# chips.append(pcv.roi.rectangle(img=img, x=320, y=170, w=dimensions[0], h=dimensions[1]),) #red
# chips.append(pcv.roi.rectangle(img=img, x=320 , y=220 , w=dimensions[0], h=dimensions[1]))#green
# chips.append(pcv.roi.rectangle(img=img, x=375 , y=50 , w=dimensions[0], h=dimensions[1]))#blue
# chips.append(pcv.roi.rectangle(img=img, x=375 , y=120 , w=dimensions[0], h=dimensions[1]))#turq
# chips.append(pcv.roi.rectangle(img=img, x=375 , y=170 , w=dimensions[0], h=dimensions[1]))#pink
# chips.append(pcv.roi.rectangle(img=img, x=375 , y=220 , w=dimensions[0], h=dimensions[1]))#orange
# mask = np.zeros(shape=np.shape(img)[:2], dtype = np.uint8()) # create empty mask img.
# #
# print(mask)
# #
# # # draw contours for each region of interest and give them unique color values.
# #
# i = 1
# for chip in chips:
#     print(chip)
#     mask = cv2.drawContours(mask, chip[0], -1, (i*10), -1)
#     i += 1
#
#
 # pcv.plot_image(mask, cmap="gray")
#
# mask = mask * 10  #multiply values in the mask for greater contrast. Exclude if designating have more than 25 color chips.
#
np.unique(mask)
pcv.print_image(img=mask, filename="/home/nadav/Pictures/mask1.png") #write to file.