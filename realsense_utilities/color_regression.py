import cv2
import numpy as np
import matplotlib.pyplot as plt
# from colour.plotting import plot_image
import colour
import skimage
from numpy.distutils.fcompiler import none
from sklearn.cross_decomposition import PLSRegression
# real colors value
reference_colors = [[115,82,68],[194,150,130],[98,122,157],[87,108,67],[133,128,177],[103,189,170],[214,126,44],[80,91,166],[193,90,99],[94,60,108],[157,188,64],[224,163,46],[56,61,150],[70,148,73],[175,54,60],[231,199,31],[187,86,149],[8,133,161],[243,243,242],[200,200,200],[160,160,160],[121,122,121],[85,85,85],[52,52,52]]
# differant exposure test values at 120
reference = [[148, 118, 113], [214, 171, 162], [102, 161, 197], [123, 150, 132], [136, 170, 210], [123, 194, 201], [228, 150, 116], [77, 143, 193], [210, 127, 138], [117, 118, 158], [179, 199, 141], [252, 187, 142], [67, 123, 174], [100, 157, 123], [180, 107, 112], [238, 199, 136], [197, 126, 163], [32, 171, 207], [220, 223, 226], [197, 202, 207], [174, 184, 189], [140, 154, 160], [109, 125, 135], [87, 99, 105]]
# light and shadow
ref = [[117, 87, 82], [193, 141, 132], [80, 126, 160], [101, 116, 98], [115, 137, 177], [102, 159, 164], [191, 113, 77], [56, 109, 158], [180, 86, 98], [89, 80, 116], [149, 155, 95], [216, 146, 98], [41, 89, 138], [76, 119, 83], [148, 66, 74], [204, 153, 87], [164, 81, 113], [19, 131, 163], [196, 190, 194], [166, 165, 169], [145, 140, 148], [110, 109, 118], [81, 83, 91], [59, 59, 63]]

# drone colors red first
ref2 = [[140, 91, 25], [141, 87, 93], [55, 99, 111], [23, 55, 78], [44, 92, 38], [118, 37, 28]]
#red last
ref3 = [[215, 146, 62], [215, 146, 62], [48, 150, 192], [215, 146, 62], [48, 150, 192], [48, 150, 192]]
test2 = [[146, 117, 113], [220, 175, 168], [103, 161, 196], [122, 148, 132], [135, 169, 209], [122, 192, 199], [227, 150, 115], [76, 143, 193], [210, 127, 139], [115, 117, 157], [181, 199, 142], [249, 186, 141], [68, 123, 174], [100, 155, 121], [180, 107, 113], [235, 197, 134], [196, 127, 162], [33, 171, 208], [218, 222, 225], [195, 201, 205], [173, 184, 189], [139, 154, 162], [110, 126, 136], [89, 99, 105]]
#vidoe red last
test1 = [[199, 44, 92], [199, 44, 92], [25, 141, 233], [199, 44, 92], [25, 141, 233], [25, 141, 233]]
def unique_count_app(a):

        r, g, b = a[:, 0], a[:, 1], a[:, 2]
        pixels2delete = (r > 200) & (g > 200) & (b > 200)
        pixels2delete = np.bitwise_not(pixels2delete)
        a = a[pixels2delete]
        common = np.mean(a.reshape((-1, 3)), axis=0).astype(np.int)
        # common = np.median(a.reshape((-1, 3)), axis=0).astype(np.int)
        # colors, count = np.unique(a.reshape(-1,a.shape[-1]), axis=0, return_counts=True)
        # return list(colors[count.argmax()])
        return list(common)
bgr = cv2.imread('/home/nadav/Desktop/expo_Color.png')
rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)
mask = cv2.imread('/home/nadav/Desktop/maskexpo.png')
# mask = cv2.cvtColor(mask1,cv2.COLOR_BGR2GRAY)

unique_mask_rgb_vals = np.unique(mask.reshape((mask.shape[0]*mask.shape[1], 3)), axis=0)
count = 1
colors = []
for unq_rgb_val in np.sort(unique_mask_rgb_vals):
    if np.all(unq_rgb_val == 0):
        continue

    # single_fruit_mask = np.all(mask == unq_bgr_val, axis=-1).astype(np.uint8) * 255
    # contours = cv2.findContours(single_fruit_mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[0][0]
    # x, y, w, h = cv2.boundingRect(contours)  # x_left, y_up, width, height
    single_fruit_mask = np.all(mask == unq_rgb_val, axis=-1).astype(np.uint8) * 255
    ys, xs = np.where(single_fruit_mask != 0)
    x, x2 = np.min(xs), np.max(xs)
    y, y2 = np.min(ys), np.max(ys)
    w = x2 - x
    h = y2 - y


    # if w < 15 or h < 15:  # TODO change to fit your needs
    #     continue

    # cv2.rectangle(bgr, (x , y ), (x + w , y + h ), (0, 255, 0), 2)
    # cv2.imshow('', bgr)
    # key = cv2.waitKey(0)
    # cv2.destroyAllWindows()
    # bgr = cv2.imread('C:/Users/Nadav/Desktop/office/exposure/e30_Color.png')
    #
    # cv2.imshow('', bgr)
    # TODO this is your crops, you can enlarge them, save them, annotate them, etc
    rgb_crop = rgb[y:y + h, x:x + w, :]

    mask_crop = single_fruit_mask[y:y + h, x:x+w]

    # mask_crop_bl = np.concatenate((np.expand_dims(mask_crop, -1), np.expand_dims(mask_crop, -1), np.expand_dims(mask_crop, -1)), axis=-1)
    # mask_crop_bl[mask_crop_bl > 0] = 1
    # rgb_crop_masked = rgb_crop * mask_crop_bl
    count += 1

    single_fruit_mask = (single_fruit_mask / 255).astype(np.uint8)
    rgb_where_mask = rgb[y:y + h, x:x + w] * \
                     np.concatenate((np.expand_dims(single_fruit_mask[y:y + h, x:x + w], 2),
                                     np.expand_dims(single_fruit_mask[y:y + h, x:x + w], 2),
                                     np.expand_dims(single_fruit_mask[y:y + h, x:x + w], 2)),
                                    axis=-1)
    rgb_crop = rgb_where_mask
    colors_vectors = rgb_where_mask.reshape((-1, 3))
    pixels = []
    for i in colors_vectors:
        if all(i != [0,0,0]):
            # colors_vectors = np.delete(colors_vectors,i)
            pixels.append(list(i))

    # cv2.imshow("d",rgb_crop)
    # cv2.waitKey()
    colors.append(unique_count_app(np.array(pixels)))

print (colors)

# pls = PLSRegression(n_components=3)
# pls.fit(colors,test1)
# print(pls.score(colors,test1))
# org_img = cv2.imread('C:/Users/Nadav/Desktop/office/exposure/e30_Color.png',cv2.COLOR_BGR2RGB)
# calibrated_img = morph = org_img.copy()
# for im in calibrated_img:
#     im[:] = pls.predict(im[:])

# import colour
# org_img = cv2.imread('/home/nadav/Pictures/test/e30_Color.png',cv2.COLOR_BGR2RGB)
# calibrated_img = morph = org_img.copy()

# index = [11,15,18]
# for i in index:
#     del colors[i]
#     del reference[i]

none
# New_reference=[]
# New_Actual_colors=[]
# for L,K in zip(colors, range(len(colors))):
#     if any(m in L for m in [0, 255]):
#         print(L, "value outside of range")
#     else:
#         New_reference.append(reference[K])
#         New_Actual_colors.append(colors[K])
#Convert image from int to float
# Float_image=skimage.img_as_float(bgr)
#
# #Normalise image to have pixel values from 0 to 1
# Normalised_image = (Float_image - np.min(Float_image))/np.ptp(Float_image)
#
# #Decoded the image with sRGB EOTF
# Decoded_img=colour.models.eotf_sRGB(Normalised_image)
#
# #Performed Finlayson 2015 color correction to linear data:
# for im in Decoded_img:
#     im[:]=colour.colour_correction(im[:],colors,test1, "Finlayson 2015")
#
# #Encoded image back to sRGB
# Encoded_img=colour.models.eotf_inverse_sRGB(Decoded_img)
#
# #Denormalized image to fit 255 pixel values
# Denormalized_image=Encoded_img*255

#Converted floats back to integers
# Integer_image= Denormalized_image.astype(int)
# cv2.imwrite('/home/nadav/Desktop/calapp1.png',Integer_image)
# for im in calibrated_img:
#     im[:] = colour.colour_correction(im[:],colors,reference,'Finlayson 2015')
# # plot_image(org_img,calibrated_img)
# cv2.imwrite('/home/nadav/Pictures/e30_Colorcal2.png',calibrated_img)
# fig=plt.figure(figsize=(8, 8))
import colour
# org_img = cv2.imread('C:/Users/Nadav/Desktop/office/exposure/e30_Color.png',cv2.COLOR_BGR2RGB)
# calibrated_img = morph = org_img.copy()
# n = 1
# extracted_color = np.array(colors)
# reference_color = np.array(reference_colors)
# for im in calibrated_img:
#     for i in range(0,im.shape[0],n):
#             im[i:i+n] = TPS_color_correction(im[i:i+n],colors,reference_colors)
# # plot_image(org_img,calibrated_img)
# cv2.imwrite('C:/Users/Nadav/Desktop/office/exposure/e30_Colorcal3.png',calibrated_img)