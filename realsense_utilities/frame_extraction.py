import cv2
import numpy as np

def save_frame(frame, path):

    cv2.imwrite(path, frame)


if __name__ == '__main__':


    vid_path = 'C:/Users/Nadav/Desktop/color cheker/sun2.avi'
    # office = [10,21,29,41,49,57,68,76]
    # shadow = [15,26,38,51,62,75,86,97]
    # sun = [12,24,33,49,59,71,79,92]
    sun2 = [7,24,35,50,63,71,81,89]
    start_frame = [i * 30 for i in sun2]

    count = 1
    for frame in start_frame:
        cap = cv2.VideoCapture(vid_path)
        cap.set(cv2.CAP_PROP_POS_FRAMES, frame)

        ret, frame = cap.read()
        if ret:

            save_frame(frame, 'C:/Users/Nadav/Desktop/color cheker/sun2%d.png' % count)

        count += 1
