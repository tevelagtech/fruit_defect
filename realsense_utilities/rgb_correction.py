from plantcv import plantcv as pcv
import cv2
import numpy as np
import matplotlib
from PIL import Image
from PIL import ImageEnhance
matplotlib.use('TKAgg')
pcv.params.debug = "plot" #set debug mode

target_img, targetpath, targetname = pcv.readimage(filename="C:/Users/Nadav/Desktop/office/exposure/e90_Color.png")
# image = Image.open("C:/Users/Nadav/Desktop/office/xrite/reds_Color.png")
# enh_bri = ImageEnhance.Brightness(image)
# brightness = 4
# image = enh_bri.enhance(brightness)
# enh_con = ImageEnhance.Contrast(image)
# contrast = 2
# # image = enh_con.enhance(contrast)
# enh_sha = ImageEnhance.Sharpness(image)
# sharpness = -2
# # image = enh_sha.enhance(sharpness)
# enh_col = ImageEnhance.Color(image)
# color = 2
# # image = enh_col.enhance(color)
# image.save("C:/Users/Nadav/Desktop/office/xrite/reds_Color_fix.png")
source_img, sourcepath, sourcename = pcv.readimage(filename="C:/Users/Nadav/Desktop/office/exposure/e270_Color.png")
mask, maskpath, maskname = pcv.readimage(filename="C:/Users/Nadav/Desktop/office/exposure/mask.png")

# mask[mask==164&108&200&152&252]=0

# mask1, maskpath1, maskname1 = pcv.readimage(filename="C:/Users/Nadav/Desktop/New folder/test_mask2.png")
#Since target_img and source_img have the same zoom and colorchecker position,
# the same mask can be used for both.
#.npz files containing target_matrix, source_matrix, and
# transformation_matrix will be saved to the output_directory file path

output_directory = "C:/Users/Nadav/Desktop/office/exposure/test1"
# Inputs:
#   target_img - RGB image with color chips
#   target_mask - Grayscale image with color chips and background each represented with unique values
#   source_img - RGB image with color chips
#   source_mask - Grayscale image with color chips and background each represented with unique values
#   output_directory - File path to which the target_matrix, source_matrix, and tranformation_matrix will be saved

target_matrix, source_matrix, transformation_matrix, corrected_img = pcv.transform.correct_color(target_img=target_img,
                                                                                                 target_mask=mask,
                                                                                                 source_img=source_img,
                                                                                                 source_mask=mask,
                                                                                                 output_directory=output_directory)
from numpy import load

data = load('C:/Users/Nadav/Desktop/office/exposure/test1/transformation_matrix.npz')
lst = data.files
for item in lst:
    print(item)
    print(data[item])
pcv.transform.quick_color_check(source_matrix=source_matrix, target_matrix=target_matrix, num_chips=24)