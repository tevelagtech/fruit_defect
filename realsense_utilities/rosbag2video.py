import pyrealsense2 as rs
import cv2
import numpy as np
import os


def split_16bit(dpt_im):
    imH = dpt_im >> 8
    imL = dpt_im & 255
    return imH.astype(np.uint8), imL.astype(np.uint8)


def read(bag_path, fps = 30, height=480, width=640):
    path2write = os.path.split(bag_path)[0]
    filename = os.path.splitext(os.path.basename(bag_path))[0]

    config = rs.config()
    rs.config.enable_device_from_file(config,bag_path)
    pipeline = rs.pipeline()

    # config.enable_stream(rs.stream.depth, width, height, rs.format.z16, fps)
    config.enable_stream(rs.stream.color, width, height, rs.format.rgb8, fps)  # COLOR

    pipeline.start(config)

    align_to = rs.stream.color
    align = rs.align(align_to)

    fourcc_rgb = cv2.VideoWriter_fourcc(*'X264')
    fourcc_rgb = cv2.VideoWriter_fourcc(*'XVID')
    # fourcc_dpt = cv2.VideoWriter_fourcc(*'mp4v')# 0
    # fourcc_dpt = cv2.VideoWriter_fourcc(*'HFYU')# 0
    # fourcc_dpt = cv2.VideoWriter_fourcc(0)# 0

    bgr_video_writer = cv2.VideoWriter(os.path.join(path2write, '{}_C.avi'.format(filename)), fourcc_rgb, fps, (width, height), True)
    # dpt_video_writer_H = cv2.VideoWriter(os.path.join(path2write, '{}_DH.avi'.format(filename)), fourcc_dpt, fps, (width, height), False)
    # dpt_video_writer_L = cv2.VideoWriter(os.path.join(path2write, '{}_DL.avi'.format(filename)), fourcc_dpt, fps, (width, height), False)

    frame_number = 0
    while True:

        frames = pipeline.wait_for_frames()
        cur_frame_number = frames.frame_number
        if cur_frame_number < frame_number:
            break

        if cur_frame_number == frame_number:
            continue

        frame_number = cur_frame_number
        aligned_frames = align.process(frames)
        # Get aligned frames
        # aligned_depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
        color_frame = aligned_frames.get_color_frame()

        # Validate that both frames are valid
        if not color_frame:
            continue

        color_image = np.asanyarray(color_frame.get_data())
        color_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2RGB)
        # depth_image = np.asanyarray(aligned_depth_frame.get_data())

        # depth_frame_int = (depth_image).astype(np.uint16)
        # dpt_im_split_h, dpt_im_split_l = split_16bit(depth_frame_int)

        # save frames
        bgr_video_writer.write(color_image)
        # dpt_video_writer_H.write(dpt_im_split_h)
        # dpt_video_writer_L.write(dpt_im_split_l)

    bgr_video_writer.release()
    # dpt_video_writer_H.release()
    # dpt_video_writer_L.release()


if __name__ == '__main__':
    bag_path = '/home/nadav/Desktop/20220301_124045_nactarine_flowers3.bag'
    read(bag_path, height=720, width=1280)

