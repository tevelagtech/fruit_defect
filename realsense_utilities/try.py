import cv2
import numpy as np
import pyrealsense2 as rs



# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
# pipeline.start(config)
cap = cv2.VideoCapture('/home/nadav/Desktop/tevel/vid_201022_131800_C.avi')
while(cap.isOpened()):
    ret, frame = cap.read()

    # frames = pipeline.wait_for_frames()
    # color_frame = frames.get_color_frame()
    # frame = np.asanyarray(color_frame.get_data())

    hsv = cv2.cvtColor(frame,cv2.COLOR_BGR2LAB)

    lower_red = np.array([0,150,0])
    upper_red = np.array([255,255,255])
    mask = cv2.inRange(hsv, lower_red, upper_red)
    res = cv2.bitwise_and(frame, frame, mask = mask)


    connectivity = 4
    num_labels, labels, stats, centroids = cv2.connectedComponentsWithStats(mask, connectivity, cv2.CV_32S)
    for stat in stats:
        [x, y, w, h, area] = stat
        if w == mask.shape[1] or h == mask.shape[0]:
            continue

        if w>10 or h>10:
            frame = cv2.rectangle(frame, (x, y), (x+w+10, y+h+10), (0, 255, 0), 1)
    a = np.double(hsv)
    b = a -30
    img2 = np.uint8(b)
    cv2.imshow('frame', frame)
    cv2.imshow('mask', mask)
    cv2.imshow('res', res)
    k = cv2.waitKey(5) & 0xFF
    if k == 27:
        break
cv2.destroyAllWindows()
# import datetime
# from time import gmtime, strftime
# strftime("%Y-%m-%d %H:%M:%S", gmtime(1604403331.76784))