import pandas as pd
from sklearn import preprocessing
from sklearn.preprocessing import StandardScaler, Normalizer

data = '/home/nadav/export_data2.csv'


# names = ['EXR','Rn','Gn','Bn','meanL(hsl)','meanH(hsl)','meanS(hsl)','meanLa(lab)',
#          'meanA(lab)','meanB(lab)','meanCb','meanCr','meanY','sdCb'	,'cive','rbi','hue angle','grade']
# names = ['meanH(hsl)','meanS(hsl)','meanLa(lab)',
#          'meanA(lab)','meanB(lab)','meanCb','meanCr','meanY','cive','hue angle','grade']
df = pd.read_csv(data,header=0)
# df.head()
# create a scaler object
std_scaler = StandardScaler()

# fit and transform the data
df_std = pd.DataFrame(std_scaler.fit_transform(df), columns=df.columns)


print(df_std.corr())
import numpy as np


def triang(cormat, triang='lower'):
    if triang == 'upper':
        rstri = pd.DataFrame(np.triu(cormat.values),
                             index=cormat.index,
                             columns=cormat.columns).round(2)
        rstri = rstri.iloc[:, 1:]
        rstri.drop(rstri.tail(1).index, inplace=True)

    if triang == 'lower':
        rstri = pd.DataFrame(np.tril(cormat.values),
                             index=cormat.index,
                             columns=cormat.columns).round(2)
        rstri = rstri.iloc[:, :-1]
        rstri.drop(rstri.head(1).index, inplace=True)

    rstri.replace(to_replace=[0, 1], value='', inplace=True)

    return (rstri)
print(triang(df.corr(), 'upper'))
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TKAgg')
cormat = df.corr(method='spearman')
mask = np.zeros_like(cormat, dtype=np.bool)
mask[np.triu_indices_from(mask)] = True

fig = plt.figure(figsize=(12, 8))
sns.heatmap(cormat, mask=mask, vmax=.5, center=0,
            square=False, linewidths=.5, cbar_kws={"shrink": .5},annot=True)
plt.show()
# Univariate Histograms
import matplotlib.pyplot as plt
import pandas
from pandas.plotting import scatter_matrix

url = "/home/nadav/Desktop/vid_211024_103552_C.csv"
# names = ['EXR','Rn','Gn','Bn','meanL(hsl)','meanH(hsl)','meanS(hsl)','meanLa(lab)',
#          'meanA(lab)','meanB(lab)','meanCb','meanCr','meanY','sdCb'	,'cive','rbi','hue angle','grade']

# df_std.hist()
# plt.show()
sns.set_context('paper', font_scale=0.6)
matplotlib.rc('xtick', labelsize=8)
matplotlib.rc('ytick', labelsize=6)
scatter_matrix(df_std)
plt.show()
