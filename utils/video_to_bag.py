import cv2
import numpy as np
import sys
import os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
from rs_grabber import VideoRecorder
from common_tools.rosbag_utils import BagReader
from os import path, listdir
import rosbag
import rospy
from shutil import copyfile
from cv_bridge import CvBridge

bridge = CvBridge()
rospy.init_node("converter")

string_topic_list = {'state':'/tevel/perception/state',
               'force':'/tevel/perception/force',
               'state_force':'/tevel/perception/stateforce',
               'log': '/rosout'}

string_topic_data = {}


def state_time(bag):
    out = {}
    time_msg =[]
    item =[]
    for msg in bag:
        time_msg.append(msg.timestamp.to_sec())
        try:
            item.append(msg.message.data)
        except:
            pass
        try:
            item.append(msg.message.msg)
        except:
            pass
    out['time'] = time_msg
    out['data'] = item
    return out


def _get_clostest_timestamp_values(required_timestamp, timestamps_array, values):

    timestamps_array = np.array(timestamps_array)
    closest_idx = np.where(np.array(timestamps_array).astype(np.float) - required_timestamp <0)[0]
    if closest_idx.size>0:
        closest_idx= closest_idx[-1]
        return values[closest_idx],float(timestamps_array[closest_idx])
    else :
        return None,0

def get_frames(video_path, start_frame=0):
    cap = cv2.VideoCapture(video_path)
    cap.set(cv2.CAP_PROP_POS_FRAMES, start_frame)

    while True:
        ret, frame = cap.read()
        yield frame


def generate_text_image(bgr, required_timestamp, bagreader):
    FONT_SIZE = 0.7
    FONT = cv2.FONT_HERSHEY_SIMPLEX
    FONT_THICKNESS = 2
    BLACK_FONT = (0, 0, 0)

    h, w, c = bgr.shape
    text_im = np.ones((h, int(w*1.5), 3)) * 255
    first_line = 30
    line_space = 35
    x_margin = 30
    line = first_line

    cv2.putText(text_im, 'Frame Time :{}'.format(required_timestamp) , (x_margin, line), FONT, FONT_SIZE,
                BLACK_FONT, FONT_THICKNESS)
    line += line_space

    for key in string_topic_data.keys():
        if len(string_topic_data[key]['time'])>0:

            msg,msg_time = _get_clostest_timestamp_values(required_timestamp, string_topic_data[key]['time'],
                                                       string_topic_data[key]['data'])
            if abs(required_timestamp - msg_time) < 0.5:
                cv2.putText(text_im,'time : {} |topic : {}| data: {}'.format(msg_time,key, msg[:35])  , (x_margin, line), FONT, FONT_SIZE, BLACK_FONT, FONT_THICKNESS)
                line += line_space
                if len(msg)>35:
                    cv2.putText(text_im, ' {}'.format( msg[35:]), (x_margin, line), FONT,
                                FONT_SIZE, BLACK_FONT, FONT_THICKNESS)
                    line += line_space
    # /mavros/local_position/velocity
    # fcu_twist_xyz = _get_clostest_timestamp_values(required_timestamp, bagreader.fcu_vel_mat[:, 0], bagreader.fcu_vel_mat[:, 1:])
    # x, y, z = fcu_twist_xyz
    # x, y, z = np.round(x, 4), np.round(y, 4), np.round(z, 4)
    # cv2.putText(text_im, '/mavros/local_position/velocity X: {}, Y: {}, Z: {}'.format(x, y, z), (x_margin, line), FONT, FONT_SIZE, BLACK_FONT, FONT_THICKNESS)
    # line += line_space
    #
    # # /mavros/local_position/odom
    # ned_pos_xyz_quat_wxyz = _get_clostest_timestamp_values(required_timestamp, bagreader.ned_pos_mat[:, 0], bagreader.ned_pos_mat[:, 1:])
    # x, y, z, quat_w, quat_x, quat_y, quat_z = ned_pos_xyz_quat_wxyz
    # x, y, z = np.round(x, 3), np.round(y, 3), np.round(z, 3)
    # quat_w, quat_x, quat_y, quat_z = np.round(quat_w, 3), np.round(quat_x, 3), np.round(quat_y, 3), np.round(quat_z, 3)
    # cv2.putText(text_im, '/mavros/local_position/odom pos X: {}, Y: {}, Z: {}'.format(x, y, z), (x_margin, line), FONT, FONT_SIZE, BLACK_FONT, FONT_THICKNESS)
    # line += line_space
    # cv2.putText(text_im, '/mavros/local_position/odom quat W: {}, X: {}, Y: {}, Z: {}'.format(quat_w, quat_x, quat_y, quat_z), (x_margin, line), FONT, FONT_SIZE, BLACK_FONT, FONT_THICKNESS)
    # line += line_space
    #
    # # /tevel/debug/pick_pos_world_fcu_raw
    # world_target_xyz = _get_clostest_timestamp_values(required_timestamp, bagreader.world_missions_raw_xyz[:, 0], bagreader.world_missions_raw_xyz[:, 1:])
    # x, y, z = world_target_xyz
    # x, y, z = np.round(x, 3), np.round(y, 3), np.round(z, 3)
    # cv2.putText(text_im, '/tevel/debug/pick_pos_world_fcu_raw X: {}, Y: {}, Z: {}'.format(x, y, z), (x_margin, line), FONT, FONT_SIZE, BLACK_FONT, FONT_THICKNESS)
    # line += line_space
    #
    return text_im


if __name__ == '__main__':
    # ----------------------------------------
    # CHANGE THIS PATH TO FULL RGB PATH
    video_full_path = '/home/nadav/docker_images/docker_shared/peaches/15_05_21/31/vid_210513_121738_C.avi'
    # create_text_image = True

    video_full_path = sys.argv[1]
    create_text_image = False
    if len(sys.argv) > 2:
        create_text_image = sys.argv[2]
        create_text_image = create_text_image.lower() == 'true'
    # ----------------------------------------

    video_dir_path = path.dirname(video_full_path)
    video_fname = path.basename(video_full_path).partition('.')[0][:-2]
    files = listdir(video_dir_path)

    rosbag_file = ""
    new_rosbag_file = ""
    for file in files:
        if ".bag" in file and file != "rosbag.bag" and 'with_video' not in file:
            rosbag_file = video_dir_path + "/" + file
            print ('reading', rosbag_file)
            new_rosbag_file = video_dir_path + "/" + file[:file.index('.bag')] + '_with_video.bag'
            copyfile(rosbag_file, new_rosbag_file)
            break

    if create_text_image:
        bagreader = BagReader(new_rosbag_file)
        # bagreader.extract_fcu_velocity()
        # bagreader.extract_missions_world_fcu_raw()
        # bagreader.extract_ned()
        for key in string_topic_list.keys():
            bag = bagreader.bag.read_messages(topics = string_topic_list[key])
            string_topic_data[key] = state_time(bag)

    if new_rosbag_file != "":
        bag = rosbag.Bag(new_rosbag_file, 'a')
        vid_recorder = VideoRecorder.Read_RGBD_Video(video_fname, video_dir_path)
        video_mata = vid_recorder.metadata
        zero_time = vid_recorder.metadata[0, 0]
        for frame_id, bgr in enumerate(get_frames(video_full_path)):
            if bgr is None:
                break
            t = vid_recorder.metadata[frame_id, 0]
            frame_time = t / 1e6

            if create_text_image:
                text_im = generate_text_image(bgr, frame_time, bagreader)
                bgr = np.hstack((bgr, text_im))

            img_msg = bridge.cv2_to_compressed_imgmsg(bgr, "jpg")
            img_msg.header.stamp = rospy.Time.from_sec(frame_time)
            img_msg.header.frame_id = str(frame_id)
            bag.write('/tevel/video/compressed', img_msg, img_msg.header.stamp)
        bag.close()
        print ('done - open new rosbag:', new_rosbag_file)
    else:
        print ('no bag was found')
